-------------------------------------------------------------------------------
--
-- Title       : PCReg
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\PCReg.vhd
-- Generated   : Fri Jun 28 17:19:31 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {PCReg} architecture {PCReg}}

library IEEE;
use IEEE.std_logic_1164.all;

entity PCReg is	
	 port(
		 NPC : in STD_LOGIC_VECTOR(31 downto 0);
		 ck2 : in STD_LOGIC;
		 CE_RI : in STD_LOGIC;
		 Rst_regs : in STD_LOGIC;
		 PC : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end PCReg;

--}} End of automatically maintained section

architecture PCReg of PCReg is
begin
Registrador:
process (ck2, Rst_regs)
-- Section above this comment may be overwritten according to
-- "Update sensitivity list automatically" option status
begin
	if Rst_regs='1' then	-- 	Reset assíncrono
		PC(32 -1 downto 0) <= (others => '0');-- Inicialização com zero
	elsif (ck2'event and ck2='1' and CE_RI='1') then  -- Clock na borda de subida
			 PC <= NPC after 1 ns;
	end if;
end process Registrador;

end PCReg;
