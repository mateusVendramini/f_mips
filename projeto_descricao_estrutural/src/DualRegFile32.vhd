-------------------------------------------------------------------------------
--
-- Title       : DualRegFile32
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\DualRegFile32.vhd
-- Generated   : Fri Jun 28 21:20:25 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {DualRegFile32} architecture {DualRegFile32}}

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity DualRegFile32 is
	 port(
		 Ent1 : in STD_LOGIC_VECTOR(20 downto 16);
		 WriteRegister : in STD_LOGIC_VECTOR(4 downto 0);
		 Ent2 : in STD_LOGIC_VECTOR(4 downto 0);
		 WriteDataBack : in STD_LOGIC_VECTOR(31 downto 0); 
		 clkmemreg : in std_logic;
		 RegWriteWBInt : in std_logic;
		 RegA : out STD_LOGIC_VECTOR(31 downto 0);
		 RegB : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end DualRegFile32;

--}} End of automatically maintained section

architecture DualRegFile32 of DualRegFile32 is	 

---- Architecture declarations -----
type ram_type is array (0 to 2**5 - 1)
        of std_logic_vector (32 - 1 downto 0);
signal ram: ram_type := (others => (others => '0'));


---- Signal declarations used on the diagram ----

signal enda_reg : std_logic_vector(5 - 1 downto 0);
signal endb_reg : std_logic_vector(5 - 1 downto 0);
signal endw_reg : std_logic_vector(5 - 1 downto 0);
begin
	
RegisterMemory :
process (clkmemreg)
begin
	 if (clkmemreg'event and clkmemreg = '1') then
        if (RegWriteWBInt = '1' and to_integer(unsigned(WriteRegister)) /= 0) then
           ram(to_integer(unsigned(WriteRegister))) <= WriteDataBack;-- after Twrite;
        end if;
        enda_reg <= Ent1;
        endb_reg <= Ent2;
     end if;
	 -- le na borda de descida
	 if (clkmemreg'event and clkmemreg = '0') then 
		 ---- User Signal Assignments ----
		RegA <= ram(to_integer(unsigned
								(enda_reg))); --after Tread;
		RegB <= ram(to_integer(unsigned
								(endb_reg))); --after Tread;
	end if;
end process RegisterMemory;



end DualRegFile32;
