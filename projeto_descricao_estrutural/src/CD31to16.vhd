-------------------------------------------------------------------------------
--
-- Title       : CD31to16
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\CD31to16.vhd
-- Generated   : Tue Jul  2 16:13:14 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {CD31to16} architecture {CD31to16}}

library IEEE;
use IEEE.std_logic_1164.all;

entity CD31to16 is
	 port(
		 EnderDados : in STD_LOGIC_VECTOR(31 downto 0);
		 Ender : out STD_LOGIC_VECTOR(15 downto 0)
	     );
end CD31to16;

--}} End of automatically maintained section

architecture CD31to16 of CD31to16 is
begin
	Ender <=  "00" & EnderDados(15 downto 2);
	 -- enter your statements here --

end CD31to16;
