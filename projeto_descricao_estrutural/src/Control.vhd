-------------------------------------------------------------------------------
--
-- Title       : Control
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Control.vhd
-- Generated   : Fri Jun 28 22:38:18 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Control} architecture {Control}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Control is
	 port(
		 RIout : in STD_LOGIC_VECTOR(31 downto 26);	   
		 Reset : in STD_LOGIC;
		 RegWrite : out STD_LOGIC := '0';
		 MemtoReg : out STD_LOGIC := '0';
		 MemWrite : out STD_LOGIC := '0';
		 MemRead : out STD_LOGIC := '0';
		 Branch : out STD_LOGIC := '0';
		 PCSrc : out STD_LOGIC := '0';
		 ALUSrc : out STD_LOGIC := '0';
		 RegDst : out STD_LOGIC := '0';
		 ALUOp : out STD_LOGIC := '0' ;
		 bneq : out STD_LOGIC := '0';
		 jal : out STD_LOGIC := '0';
		 ALUOp2 : out STD_LOGIC := '0';
		 Rst_regs : out STD_LOGIC := '0';
		 ClockControl : out STD_LOGIC
	     );
end Control;

--}} End of automatically maintained section

architecture Control of Control is 
signal ck : std_logic := '1';
begin

ck <= not ck after 5 ns;  
ClockControl <= ck;

Rst_regs <= Reset;

-- default values


-- rom hardcoded
control_process: 
process(RIout)
begin

	case RIout	is
		when "000000" =>  RegWrite <= '1' ;	 --R type
						 ALUOp <= '1';
						 ALUOp2 <= '0'; 
						 MemtoReg <= '0'; 	
						 MemWrite <= '0';	
						 MemRead  <= '0';	
						 Branch	  <= '0'; 
						 PCSrc 	  <= '0';	
						 ALUSrc   <= '0';	
						 RegDst	  <= '0'; 			
						 bneq 	  <= '0';	
						 jal      <= '0';										  	
         when "000001" =>  jal <= '0' ;
         when "000010" =>  --go ahead and jump
		 PCSrc <= '1';	 
		 RegWrite <= '0' ;
		 ALUOp <= '0';
		 ALUOp2 <= '0'; 
		 MemtoReg <= '0'; 	
		 MemWrite <= '0';	
		 MemRead  <= '0';	
		 Branch	  <= '0'; 
		 ALUSrc   <= '0';	
		 RegDst	  <= '0'; 			
		 bneq 	  <= '0';	
		 jal      <= '0';
         when "000011" =>  jal <= '1' ;	  --jal
		 RegWrite <= '1';  
		 ALUOp <= '0';
		 ALUOp2 <= '0';
		 MemtoReg <= '0'; 	
		 MemWrite <= '0';	
		 MemRead  <= '0';	
		 Branch	  <= '0'; 
		 PCSrc 	  <= '0';	
		 ALUSrc   <= '0';	
		 RegDst	  <= '0'; 			
		 bneq 	  <= '0';	
		 --RegDst <=  
         when "000100" => 
		 RegWrite <= '0';
		 ALUSrc <= '0';
		 Branch  <= '1' ; 	 --beq
		 ALUOp <= '0';
		 ALUOp2 <= '0';
		 MemtoReg <= '0'; 	
		 MemWrite <= '0';	
		 MemRead  <= '0';	
		 PCSrc 	  <= '0';	
		 RegDst	  <= '0'; 			
		 bneq 	  <= '0';	
		 jal      <= '0';
         when "000101" =>  --bne	
		 ALUSrc <= '0';
		 Branch <= '0';	
		 RegWrite <= '0';
		 bneq <= '1';  
		 ALUOp <= '0';
		 ALUOp2 <= '0';	
		 MemtoReg <= '0'; 	
		 MemWrite <= '0';	
		 MemRead  <= '0';	
		 PCSrc 	  <= '0';	
		 RegDst	  <= '0'; 				
		 jal      <= '0';					
         when "000110" =>  jal <= '0' ;
         when "000111" =>  jal <= '0' ;
         when "001000" => RegWrite <= '1' ;	
						 ALUOp <= '0';
						 ALUOp2 <= '1'; 
						 MemtoReg <= '0'; 	
						 MemWrite <= '0';	
						 MemRead  <= '0';	
						 Branch	  <= '0'; 
						 PCSrc 	  <= '0';	
						 ALUSrc   <= '1';	
						 RegDst	  <= '1'; 			
						 bneq 	  <= '0';	
						 jal      <= '0';				
         when "001001" =>  jal <= '0' ;
         when "001010" =>  --slti
		 ALUSrc <= '1';
		 RegWrite <= '1'; 
		 ALUOp <= '1';
		 ALUOp2 <= '1';
		 MemtoReg <= '0'; 	
		 MemWrite <= '0';	
		 MemRead  <= '0';	
		 Branch	  <= '0'; 
		 PCSrc 	  <= '0';	
		 RegDst	  <= '1'; 			
		 bneq 	  <= '0';	
		 jal      <= '0';					 
         when "001011" =>  jal <= '0' ;
         when "001100" =>  jal <= '0' ;
         when "001101" =>  jal <= '0' ;
         when "001110" =>  jal <= '0' ;
         when "001111" =>  jal <= '0' ;
         when "010000" =>  jal <= '0' ;
         when "010001" =>  jal <= '0' ;
         when "010010" =>  jal <= '0' ;
         when "010011" =>  jal <= '0' ;
         when "010100" =>  jal <= '0' ;
         when "010101" =>  jal <= '0' ;
         when "010110" =>  jal <= '0' ;
         when "010111" =>  jal <= '0' ;
         when "011000" =>  jal <= '0' ;
         when "011001" =>  jal <= '0' ;
         when "011010" =>  jal <= '0' ;
         when "011011" =>  jal <= '0' ;
         when "011100" =>  jal <= '0' ;
         when "011101" =>  jal <= '0' ;
         when "011110" =>  jal <= '0' ;
         when "011111" =>  jal <= '0' ;
         when "100000" =>  jal <= '0' ;
         when "100001" =>  jal <= '0' ;
         when "100010" =>  jal <= '0' ;
         when "100011" => 	--lw
		 ALUSrc <= '1';	
		 MemRead <= '1';
		 MemtoReg <= '1';  
		 RegWrite <= '1';
		 ALUOp <= '0';
		 ALUOp2 <= '1';	 
		 RegDst	  <= '1';
		 
		 Branch  <= '0' ; 	 
		  	
		 MemWrite <= '0';	
		 PCSrc 	  <= '0';				
		 bneq 	  <= '0';	
		 jal      <= '0';					
         when "100100" =>  jal <= '0' ;
         when "100101" =>  jal <= '0' ;
         when "100110" =>  jal <= '0' ;
         when "100111" =>  jal <= '0' ;
         when "101000" =>  jal <= '0' ;
         when "101011" => --sw	
		 ALUSrc <= '1';
		 MemWrite <= '1'; 
		 MemRead <= '0';
		 ALUOp <= '0';
		 ALUOp2 <= '1';	 
		 RegWrite <= '0';
		 RegDst	  <= '0'; 
		 Branch  <= '0' ; 	 
		 MemtoReg <= '0'; 	
		 PCSrc 	  <= '0';				
		 bneq 	  <= '0';	
		 jal      <= '0';					
         when "101010" =>  jal <= '0' ;
         when "101001" =>  jal <= '0' ;
         when "101100" =>  jal <= '0' ;
         when "101101" =>  jal <= '0' ;
         when "101110" =>  jal <= '0' ;
         when "101111" =>  jal <= '0' ;
         when "110000" =>  jal <= '0' ;
         when "110001" =>  jal <= '0' ;
         when "110010" =>  jal <= '0' ;
         when "110011" =>  jal <= '0' ;
         when "110100" =>  jal <= '0' ;
         when "110101" =>  jal <= '0' ;
         when "110110" =>  jal <= '0' ;
         when "110111" =>  jal <= '0' ;
         when "111000" =>  jal <= '0' ;
         when "111001" =>  jal <= '0' ;
         when "111010" =>  jal <= '0' ;
         when "111011" =>  jal <= '0' ;
         when "111100" =>  jal <= '0' ;
         when "111101" =>  jal <= '0' ;
         when "111110" =>  jal <= '0' ;
         when "111111" =>  jal <= '0' ;	  
		 when others =>   jal <= '0' ;
	end case;  
end process control_process;
	

end Control;
