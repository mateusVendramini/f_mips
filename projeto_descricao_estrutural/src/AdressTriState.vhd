-------------------------------------------------------------------------------
--
-- Title       : AdressTriState
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\AdressTriState.vhd
-- Generated   : Tue Jul  2 16:22:28 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {AdressTriState} architecture {AdressTriState}}

library IEEE;
use IEEE.std_logic_1164.all;

entity AdressTriState is
	 port(
		 WriteData : in STD_LOGIC_VECTOR(31 downto 0);
		 rw : in STD_LOGIC;
		 DadoEscrita : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end AdressTriState;

--}} End of automatically maintained section

architecture AdressTriState of AdressTriState is
begin
process(WriteData, rw)
begin
	if rw = '1' then
		DadoEscrita <= WriteData;
	else
		DadoEscrita <= (others => 'Z');
	end if;
end process;
	 -- enter your statements here --

end AdressTriState;
