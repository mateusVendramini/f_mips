-------------------------------------------------------------------------------
--
-- Title       : Concatena
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Concatena.vhd
-- Generated   : Sat Jun 29 15:26:44 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Concatena} architecture {Concatena}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Concatena is
	 port(
		 ALUOpExec : in STD_LOGIC;
		 ALUOpExec2 : in STD_LOGIC;
		 ALUOper : out STD_LOGIC_VECTOR(1 downto 0)
	     );
end Concatena;

--}} End of automatically maintained section

architecture Concatena of Concatena is
begin
	 ALUOper <=  ALUOpExec & ALUOpExec2;
	 -- enter your statements here --

end Concatena;
