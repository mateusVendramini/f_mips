-------------------------------------------------------------------------------
--
-- Title       : Fub7
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Fub7.vhd
-- Generated   : Sat Jun 29 21:08:53 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Fub7} architecture {Fub7}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Stall_Control is
	 port(
		 hitDados : in STD_LOGIC;
		 hitEnd : in STD_LOGIC;	  
		 PCSrcOut : in STD_logic;
		 Rst_regs : in STD_LOGIC;
		 CE_EX_MEM : out STD_LOGIC;
		 rst_mem_wb : out STD_LOGIC;
		 CE_MEM_WB : out STD_LOGIC;
		 rst_ex_mem : out STD_LOGIC;
		 CE_ID_EX : out STD_LOGIC;
		 CE_RI : out STD_LOGIC;
		 rst_if_id : out STD_LOGIC;
		 rst_id_ex : out STD_LOGIC
	     );
end Stall_Control;



architecture Stall_Control of Stall_Control is
begin  
reset_process:
process(Rst_regs, PCSrcOut, hitEnd, hitDados)
begin
if Rst_regs = '1' then
	rst_id_ex <=  '1';
	rst_ex_mem <= '1';	
	rst_if_id <= '1';	
	rst_mem_wb <= '1';
else   
	if PCSrcOut = '1' then
		rst_if_id <= '1';
		rst_id_ex <= '1';
		rst_mem_wb <= '0';
	else 		
		if hitEnd = '0' then
			rst_if_id <= '1';
		else 
			rst_if_id <= '0';
		end if;	   
		rst_mem_wb <= '0';
		rst_id_ex <= '0';
	end if;
	rst_ex_mem <= '0';	
end if;
end process reset_process;


conn_process:
process(hitEnd, hitDados, PCSrcOut)
begin  
	if ( (hitEnd = '0' or  hitDados = '0') and PCSrcOut = '0') then
		CE_RI <=  '0';
	else
		CE_RI <=  '1';
	end if;
end process conn_process;


reset_wb_process:
process(hitDados)
begin
	if hitDados = '0' then
		CE_MEM_WB <= '0'; 
		CE_EX_MEM <= '0';
		CE_ID_EX <= '0';
	else 
		CE_MEM_WB <= '1'; 
		CE_EX_MEM <= '1';
		CE_ID_EX <= '1';
	end if;
		
end process reset_wb_process;

end Stall_Control;
