-------------------------------------------------------------------------------
--
-- Title       : SinaisGeradosProc
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\SinaisGeradosProc.vhd
-- Generated   : Tue Jul  2 15:16:50 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {SinaisGeradosProc} architecture {SinaisGeradosProc}}

library IEEE;
use IEEE.std_logic_1164.all;

entity SinaisGeradosProc is
	 port(
		 hitEnd : out STD_LOGIC;
		 hitDados : out STD_LOGIC;
		 rwi : out STD_LOGIC;
		 enablei : out STD_LOGIC
	     );
end SinaisGeradosProc;

--}} End of automatically maintained section

architecture SinaisGeradosProc of SinaisGeradosProc is
begin
	hitEnd <= '1';
	hitDados <= '1';
	rwi <= '0';
	enablei <= '1';
	 -- enter your statements here --

end SinaisGeradosProc;
