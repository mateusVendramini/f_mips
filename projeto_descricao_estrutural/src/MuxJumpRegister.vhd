library IEEE;
use IEEE.std_logic_1164.all;

entity MuxJumpRegister is
	 port(
		 NPC_Exec : in STD_LOGIC_VECTOR(31 downto 0);
		 ALU_Result : in STD_LOGIC_VECTOR(31 downto 0);
		 jalExec : in STD_LOGIC;
		 ExecResult : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end MuxJumpRegister;

--}} End of automatically maintained section

architecture MuxJumpRegister of MuxJumpRegister is
begin

TESTE:	
process (NPC_Exec, ALU_Result, jalExec)  
	begin
		Case jalExec is
		when '1' => ExecResult <= NPC_Exec;
		when '0' => ExecResult <= ALU_Result;
		when others => ExecResult <= (others => 'X');
	end case;
end process TESTE;

end MuxJumpRegister;
