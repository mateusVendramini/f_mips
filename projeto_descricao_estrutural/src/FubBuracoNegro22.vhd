-------------------------------------------------------------------------------
--
-- Title       : FubBuracoNegro22
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\FubBuracoNegro22.vhd
-- Generated   : Tue Jul  2 16:13:58 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {FubBuracoNegro22} architecture {FubBuracoNegro22}}

library IEEE;
use IEEE.std_logic_1164.all;

entity FubBuracoNegro22 is
	 port(
		 pronto : in STD_LOGIC
	     );
end FubBuracoNegro22;

--}} End of automatically maintained section

architecture FubBuracoNegro22 of FubBuracoNegro22 is
begin

	 -- enter your statements here --

end FubBuracoNegro22;
