-------------------------------------------------------------------------------
--
-- Title       : BuracoNegro
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\BuracoNegro.vhd
-- Generated   : Tue Jul  2 15:22:04 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {BuracoNegro} architecture {BuracoNegro}}

library IEEE;
use IEEE.std_logic_1164.all;

entity BuracoNegro is
	 port(
		 pronto : in STD_LOGIC
	     );
end BuracoNegro;

--}} End of automatically maintained section

architecture BuracoNegro of BuracoNegro is
begin

	 -- enter your statements here --

end BuracoNegro;
