-------------------------------------------------------------------------------
--
-- Title       : SinaisCI
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\SinaisCI.vhd
-- Generated   : Tue Jul  2 14:52:34 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {SinaisCI} architecture {SinaisCI}}

library IEEE;
use IEEE.std_logic_1164.all;

entity SinaisCI is
	 port(
		 pronto : in STD_LOGIC;
		 hitEnd : out STD_LOGIC;
		 enable : out STD_LOGIC;
		 rw : out STD_LOGIC
	     );
end SinaisCI;

--}} End of automatically maintained section

architecture SinaisCI of SinaisCI is
begin
	hitEnd <= '1';
	enable <= '1';
	rw <= '0';
	 -- enter your statements here --

end SinaisCI;
