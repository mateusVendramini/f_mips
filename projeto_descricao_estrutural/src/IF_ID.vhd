-------------------------------------------------------------------------------
--
-- Title       : IF_ID
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\IF_ID.vhd
-- Generated   : Fri Jun 28 17:51:00 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {IF_ID} architecture {IF_ID}}

library IEEE;
use IEEE.std_logic_1164.all;

entity IF_ID is
	 port(
		 NPC_IF : in STD_LOGIC_VECTOR(31 downto 0);
		 RI : in STD_LOGIC_VECTOR(31 downto 0);
		 ck1 : in STD_LOGIC;
		 CE_RI : in STD_LOGIC; 
		 rst_if_id : in STD_logic;
		 RIout : out STD_LOGIC_VECTOR(31 downto 0);
		 NPC_ID : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end IF_ID;

--}} End of automatically maintained section

architecture IF_ID of IF_ID is
begin

	Registrador:
process (ck1, rst_if_id)
-- Section above this comment may be overwritten according to
-- "Update sensitivity list automatically" option status
begin
	if rst_if_id='1' then	-- 	Reset assíncrono
		RIout(32 -1 downto 0) <= (others => '0');-- Inicialização com zero 
		NPC_ID(32 -1 downto 0) <= (others => '0');-- Inicialização com zero
	elsif (ck1'event and ck1='1' and CE_RI='1') then  -- Clock na borda de subida
		RIout <= RI after 1 ns;  
		NPC_ID <= NPC_IF after 1 ns;
	end if;
end process Registrador;

end IF_ID;
