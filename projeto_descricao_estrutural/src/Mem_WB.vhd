-------------------------------------------------------------------------------
--
-- Title       : Mem_WB
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Mem_WB.vhd
-- Generated   : Sat Jun 29 16:49:33 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Mem_WB} architecture {Mem_WB}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Mem_WB is
	 port(
		 ReadData : in STD_LOGIC_VECTOR(31 downto 0);
		 RegDstMem : in STD_LOGIC_VECTOR(4 downto 0);
		 AdressOut : in STD_LOGIC_VECTOR(31 downto 0);
		 MemtoRegMem : in STD_LOGIC;
		 RegWriteMem : in STD_LOGIC;
		 rst_mem_wb : in STD_LOGIC;
		 CE_MEM_WB : in STD_LOGIC;
		 ck7 : in STD_LOGIC;
		 ReadDataBack : out STD_LOGIC_VECTOR(31 downto 0);
		 ALUData : out STD_LOGIC_VECTOR(31 downto 0);
		 WriteRegister : out STD_LOGIC_VECTOR(4 downto 0);
		 MemtoRegWB : out STD_LOGIC;
		 RegWriteWB : out STD_LOGIC
	     );
end Mem_WB;

--}} End of automatically maintained section

architecture Mem_WB of Mem_WB is
begin

Registrador:
process (ck7, rst_mem_wb)
-- Section above this comment may be overwritten according to
-- "Update sensitivity list automatically" option status
begin
	if rst_mem_wb='1' then	-- 	Reset assíncrono
		ReadDataBack(31 downto 0)	<= (others => '0');
		ALUData(31 downto 0)		<= (others => '0');
		WriteRegister(4 downto 0)	<= (others => '0');
		MemtoRegWB					<= '0';
		RegWriteWB					<= '0';
	elsif (ck7'event and ck7='1' and CE_MEM_WB='1') then  -- Clock na borda de subida
		ReadDataBack(31 downto 0)	<= ReadData after 1 ns;
		ALUData(31 downto 0)		<= AdressOut after 1 ns;
		WriteRegister(4 downto 0)	<= RegDstMem after 1 ns;
		MemtoRegWB					<= MemtoRegMem after 1 ns;
		RegWriteWB					<= RegWriteMem after 1 ns;      
	end if;

end process Registrador;

end Mem_WB;
