-------------------------------------------------------------------------------
--
-- Title       : CalculaJump
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\CalculaJump.vhd
-- Generated   : Sat Jun 29 14:01:22 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {CalculaJump} architecture {CalculaJump}}

library IEEE;
use IEEE.std_logic_1164.all;

entity CalculaJump is
	 port(
		 JumpOperand : in STD_LOGIC_VECTOR(25 downto 0);
		 NPC_Exec : in STD_LOGIC_VECTOR(31 downto 0);
		 JumpAdress : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end CalculaJump;

--}} End of automatically maintained section

architecture CalculaJump of CalculaJump is
begin
	 JumpAdress <= 	NPC_Exec(31 downto 28) &  JumpOperand & '0' & '0';
	 -- enter your statements here --

end CalculaJump;
