-------------------------------------------------------------------------------
--
-- Title       : SignExtend
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\SignExtend.vhd
-- Generated   : Fri Jun 28 19:50:11 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {SignExtend} architecture {SignExtend}}

library IEEE;
use IEEE.std_logic_1164.all;

entity SignExtend is
	 port(
		 RIout : in STD_LOGIC_VECTOR(15 downto 0);
		 Extended : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end SignExtend;

--}} End of automatically maintained section

architecture SignExtend of SignExtend is
begin
	  Extended <=  RIout(15) & RIout(15) & RIout(15) & RIout(15) &  RIout(15) &  RIout(15) & RIout(15) & RIout(15) & RIout(15) &  RIout(15) &  RIout(15) & RIout(15) & RIout(15) & RIout(15) &  RIout(15) & RIout(15) & RIout after 1 ns;
	 

end SignExtend;
