-------------------------------------------------------------------------------
--
-- Title       : ADD
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\ADD.vhd
-- Generated   : Sat Jun 29 16:44:10 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ADD} architecture {ADD}}

library IEEE;
use IEEE.std_logic_1164.all;  
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;

entity ADD is
	 port(
		 ExtendedShifted : in STD_LOGIC_VECTOR(31 downto 0);
		 NPC_Exec : in STD_LOGIC_VECTOR(31 downto 0);
		 NPC_add : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end ADD;

--}} End of automatically maintained section

architecture ADD of ADD is
begin

	 NPC_add <= NPC_Exec + ExtendedShifted;

end ADD;
