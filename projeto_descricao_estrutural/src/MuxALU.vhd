-------------------------------------------------------------------------------
--
-- Title       : MuxALU
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\MuxALU.vhd
-- Generated   : Sat Jun 29 16:39:42 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {MuxALU} architecture {MuxALU}}

library IEEE;
use IEEE.std_logic_1164.all;

entity MuxALU is
	 port(
		 RegBEX : in STD_LOGIC_VECTOR(31 downto 0);
		 operand : in STD_LOGIC_VECTOR(31 downto 0);
		 ALUSrcExec : in STD_LOGIC;
		 ULA_in2 : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end MuxALU;

--}} End of automatically maintained section

architecture MuxALU of MuxALU is
begin
	
TESTE:	
process (RegBEX, operand, ALUSrcExec)  
	begin
		Case ALUSrcExec is
		when '0' => ULA_in2 <= RegBEX;
		when '1' => ULA_in2 <= operand;
		when others => ULA_in2 <= (others => 'X');
	end case;
end process TESTE;

end MuxALU;

