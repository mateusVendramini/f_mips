-------------------------------------------------------------------------------
--
-- Title       : EX_MEM
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\EX_MEM.vhd
-- Generated   : Sat Jun 29 16:46:08 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {EX_MEM} architecture {EX_MEM}}

library IEEE;
use IEEE.std_logic_1164.all;

entity EX_MEM is
	 port(
		 RegDstOut : in STD_LOGIC_VECTOR(4 downto 0);
		 Zero : in STD_LOGIC;
		 RegWriteExec : in STD_LOGIC;
		 MemtoRegExec : in STD_LOGIC;
		 MemWriteExec : in STD_LOGIC;
		 MemReadExec : in STD_LOGIC;
		 BranchExec : in STD_LOGIC;
		 PCJumpJRExec : in STD_LOGIC;
		 bneqExec : in STD_LOGIC;
		 NPCExecFinal : in STD_LOGIC_VECTOR(31 downto 0);
		 ExecResult : in STD_LOGIC_VECTOR(31 downto 0);
		 RegBEX : in STD_LOGIC_VECTOR(31 downto 0);
		 rst_ex_mem : in STD_LOGIC;
		 ck6 : in STD_LOGIC;
		 CE_EX_MEM : in STD_LOGIC;
		 WriteData : out STD_LOGIC_VECTOR(31 downto 0);
		 RegDstMem : out STD_LOGIC_VECTOR(4 downto 0);
		 NPCJ : out STD_LOGIC_VECTOR(31 downto 0);
		 AdressOut : out STD_LOGIC_VECTOR(31 downto 0);
		 Zero_Mem : out STD_LOGIC;
		 Branch_Mem : out STD_LOGIC;
		 PCJump_Mem : out STD_LOGIC;
		 Read : out STD_LOGIC;
		 Write : out STD_LOGIC;
		 MemtoRegMem : out STD_LOGIC;
		 RegWriteMem : out STD_LOGIC;
		 bneq_Mem : out STD_LOGIC
	     );
end EX_MEM;

--}} End of automatically maintained section

architecture EX_MEM of EX_MEM is
begin

Registrador:
process (ck6, rst_ex_mem)
-- Section above this comment may be overwritten according to
-- "Update sensitivity list automatically" option status
begin
	if rst_ex_mem='1' then	-- 	Reset assíncrono
		WriteData(31 downto 0)		<= (others => '0');
		RegDstMem(4 downto 0)		<= (others => '0');
		NPCJ(31 downto 0)			<= (others => '0');
		AdressOut(31 downto 0)		<= (others => '0');
		Zero_Mem		<= '0';
		Branch_Mem		<= '0';
		PCJump_Mem		<= '0';
		Read			<= '0';
		Write			<= '0';
		MemtoRegMem		<= '0';
		RegWriteMem		<= '0';
		bneq_Mem		<= '0'; 
			
	elsif (ck6'event and ck6='1' and CE_EX_MEM='1') then  -- Clock na borda de subida
		WriteData		<= RegBEX after 1 ns;
		RegDstMem		<= RegDstOut after 1 ns;
		NPCJ			<= NPCExecFinal after 1 ns;
		AdressOut		<= ExecResult;-- after 1 ns;
		Zero_Mem		<= Zero after 1 ns;
		Branch_Mem		<= BranchExec after 1 ns;
		PCJump_Mem		<= PCJumpJRExec after 1 ns;
		Read			<= MemReadExec after 1 ns;
		Write			<= MemWriteExec after 1 ns;
		MemtoRegMem		<= MemtoRegExec after 1 ns;
		RegWriteMem		<= RegWriteExec after 1 ns;
		bneq_Mem		<= bneqExec after 1 ns;        
	end if;
end process Registrador;


end EX_MEM;

