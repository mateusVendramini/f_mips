-------------------------------------------------------------------------------
--
-- Title       : MuxPC2
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\MuxPC2.vhd
-- Generated   : Fri Jun 28 19:09:42 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {MuxPC2} architecture {MuxPC2}}

library IEEE;
use IEEE.std_logic_1164.all;

entity MuxPC2 is
	 port(
		 NPC_IF : in STD_LOGIC_VECTOR(31 downto 0);
		 NPCJ : in STD_LOGIC_VECTOR(31 downto 0);
		 PCSrcOut : in STD_LOGIC;
		 NPC : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end MuxPC2;

--}} End of automatically maintained section

architecture MuxPC2 of MuxPC2 is
begin

TESTE:	
process (NPC_IF, NPCJ, PCSrcOut)  
begin
	Case PCSrcOut is
	when '0' => NPC <= NPC_IF after 1 ns;
	when '1' => NPC <= NPCJ after 1 ns;
	when others => NPC <= (others => 'X');
	end case;
end process TESTE;

end MuxPC2;
