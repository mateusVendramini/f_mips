-------------------------------------------------------------------------------
--
-- Title       : ResetUnit
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\ResetUnit.vhd
-- Generated   : Tue Jul  2 12:06:25 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ResetUnit} architecture {ResetUnit}}

library IEEE;
use IEEE.std_logic_1164.all;

entity ResetUnit is
	 port(
		 ResetOut : out STD_LOGIC;
		 Reset : out STD_LOGIC
	     );
end ResetUnit;

--}} End of automatically maintained section

architecture ResetUnit of ResetUnit is
begin 
reset_process:
process 
variable inicio : std_logic := '1';
begin
	if inicio = '1' then
		ResetOut <= '1';
		Reset <= '1';
		wait for 9ns;
		ResetOut <= '0';
		Reset <= '0'; 
		inicio := '0';
		
	end if;
	 -- enter your statements here --
end process reset_process;
end ResetUnit;
