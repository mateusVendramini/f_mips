-------------------------------------------------------------------------------
--
-- Title       : ALU_Control
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\ALU_Control.vhd
-- Generated   : Sat Jun 29 14:44:07 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ALU_Control} architecture {ALU_Control}}

library IEEE;
use IEEE.std_logic_1164.all; 
use IEEE.numeric_std.all;

entity ALU_Control is
	 port(
		 operand : in STD_LOGIC_VECTOR(5 downto 0);
		 ALUOper : in STD_LOGIC_VECTOR(1 downto 0);
		 --ALUOpExec2 : in STD_LOGIC;
		 ALUSel : out STD_LOGIC_VECTOR(2 downto 0);
		 JumpRegSel : out STD_LOGIC
	     );
end ALU_Control;

--}} End of automatically maintained section

architecture ALU_Control of ALU_Control is 
--signal alu_oper : std_logic_vector (1 downto 0) := 	(ALUOpExec &  ALUOpExec2);
begin
alu_control_process:
process(operand, ALUOper)
begin	
case ALUOper  	is
		when "00" =>  ALUSel  <= "000";   --sub oper
					  JumpRegSel <= '0';
		when "01" =>  ALUSel <= "001";	JumpRegSel <= '0';	--add oper
		when "11" =>  ALUSel <= "011";	JumpRegSel <= '0';  --slt oper
		when "10" =>  --R type
		case operand is
			when "100000" => ALUSel <= "001"; JumpRegSel <= '0';--add
			when "101010" => ALUSel <= "011"; JumpRegSel <= '0';	   --slt
			when "001000" => ALUSel <="101" ;JumpRegSel <= '1';	--jr
			when "100001" => ALUSel <= "100"; JumpRegSel <= '0'; 	   --addu
			when "000000" => ALUSel <="010" ;  JumpRegSel <= '0';	 --sll
			when others => ALUSel  <= "000";
						JumpRegSel <= '0';
		end case;
		
		when others => 	ALUSel  <= "000";
						JumpRegSel <= '0';
	end case;
	
	
end process alu_control_process;

end ALU_Control;
