-------------------------------------------------------------------------------
--
-- Title       : Soma4
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Soma4.vhd
-- Generated   : Fri Jun 28 17:31:47 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Soma4} architecture {Soma4}}

library IEEE;
use IEEE.std_logic_1164.all;  
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;

entity Soma4 is
	 port(
		 PC : in std_logic_vector(31 downto 0);
		 NPC_IF : out std_logic_vector(31 downto 0)
	     );
end Soma4;

--}} End of automatically maintained section

architecture Soma4 of Soma4 is
begin
	 NPC_IF <= 	PC + 4 after 2 ns;

end Soma4;
