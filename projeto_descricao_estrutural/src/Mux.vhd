-------------------------------------------------------------------------------
--
-- Title       : Mux
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Mux.vhd
-- Generated   : Sat Jun 29 16:50:19 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Mux} architecture {Mux}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Mux is
	 port(
		 ReadDataBack : in STD_LOGIC_VECTOR(31 downto 0);
		 ALUData : in STD_LOGIC_VECTOR(31 downto 0);
		 MemtoRegWB : in STD_LOGIC;
		 WriteDataBack : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end Mux;

--}} End of automatically maintained section

architecture Mux of Mux is
begin

TESTE:	
process (ReadDataBack, ALUData, MemtoRegWB)  
	begin
		Case MemtoRegWB is
		when '1' => WriteDataBack <= ReadDataBack;
		when '0' => WriteDataBack <= ALUData;
		when others => WriteDataBack <= (others => 'X');
	end case;
end process TESTE;

end Mux;
