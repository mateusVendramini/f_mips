-------------------------------------------------------------------------------
--
-- Title       : HitUnitInst
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\HitUnitInst.vhd
-- Generated   : Tue Jul  2 14:45:54 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {HitUnitInst} architecture {HitUnitInst}}

library IEEE;
use IEEE.std_logic_1164.all;

entity HitUnitInst is
	 port(
	 hitEnd : out STD_LOGIC;
	 enable : out STD_LOGIC;
	 rw     : out STD_LOGIC;
	 pronto : in STD_LOGIC
	     );
end HitUnitInst;

--}} End of automatically maintained section

architecture HitUnitInst of HitUnitInst is
begin
	hitEnd <= '1';
	enable <= '1';
	 -- enter your statements here --

end HitUnitInst;
