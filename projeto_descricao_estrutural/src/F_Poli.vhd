-------------------------------------------------------------------------------
--
-- Title       : F_Poli
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\mips\f_mips\projeto_descricao_estrutural\src\F_Poli.vhd
-- Generated   : Tue Jul  2 00:55:11 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {F_Poli} architecture {F_Poli}}

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
entity F_Poli is
	 port(
		 Clock : in STD_LOGIC;
		 ResetOut : in STD_LOGIC;
		 enable : in STD_LOGIC;
		 rw : in STD_LOGIC;
		 Ender : in STD_LOGIC_VECTOR(31 downto 0);
		 Inter : out STD_LOGIC;
		 pronto : out STD_LOGIC;
		 Dados : inout STD_LOGIC_VECTOR(31 downto 0)
	     );
end F_Poli;

--}} End of automatically maintained section

architecture F_Poli of F_Poli is	
signal RA : std_logic_vector(31 downto 0) := x"00000000";
signal RB : std_logic_vector(31 downto 0) := x"00000000";
signal Rst : std_logic_vector(31 downto 0) := x"00000000";
signal Rcmd : std_logic_vector(31 downto 0) := x"00000000";
signal Re : std_logic_vector(31 downto 	0) := x"00000000";
begin

	simulated_process:
	process(Clock, ResetOut, enable, rw, Ender) 
	variable bit : std_logic_vector(31 downto 0);
	variable res : std_logic_vector(31 downto 0);
	begin 	
		case Ender is 
			when x"000000F8" =>  RA <= Dados;
			when x"000000F9" =>  RB <= Dados;
			when x"000000FB" =>  Rcmd<= Dados;
			if Dados = x"00000001" then	
				Re <= x"00000000";
				Rst <= std_logic_vector((signed(RA(15 downto 0)))*(signed(RB(15 downto 0)))); 
				Re <= x"00000000";
			elsif 	Dados = x"00000011"	then 
				Re <= x"00000000";
				while bit > RA loop	 -- from https://en.m.wikipedia.org/wiki/Methods_of_computing_square_roots#Binary_numeral_system_.28base_2.29
					bit := std_logic_vector(shift_right(signed(bit), 2));
				end loop; 
				
				--while to_integer(signed(bit)) /= 0 loop 
				--	if to_integer(Ra) > to_integer(res) + to_integer(bit) then
				--		RA <= std_logic_vector(to_integer(RA) -  to_integer(res) + to_integer(bit));
				--		res := std_logic_vector(shift_right(to_integer(signed(res)),1) + to_integer(signed(bit))); 
				--	else 
				--		res := shift_right(res,1);
				--	end if;
				--	bit := std_logic_vector(shift_right(to_integer(signed(bit)),2));
				--end loop;
			end if;	
			--wait until Clock'event;
			Re <= x"00000001"; 
			Inter <= '1';
			when x"000000FA" => Dados <= Rst;
			when x"000000FC" => Dados <= Re;
		    when others => 
		end case;	
				
				
		
	end process simulated_process;

end F_Poli;
