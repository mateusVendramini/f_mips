-------------------------------------------------------------------------------
--
-- Title       : SL2
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\SL2.vhd
-- Generated   : Sat Jun 29 14:07:45 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {SL2} architecture {SL2}}

library IEEE;
use IEEE.std_logic_1164.all;

entity SL2 is
	 port(
		 operand : in STD_LOGIC_VECTOR(31 downto 0);
		 ExtendedShifted : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end SL2;

--}} End of automatically maintained section

architecture SL2 of SL2 is
begin

	 ExtendedShifted <= operand(29 downto 0) & '0' & '0';

end SL2;
