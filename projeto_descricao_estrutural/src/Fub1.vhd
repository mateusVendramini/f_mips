-------------------------------------------------------------------------------
--
-- Title       : Fub1
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\Fub1.vhd
-- Generated   : Sat Jun 29 15:20:18 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Fub1} architecture {Fub1}}

library IEEE;
use IEEE.std_logic_1164.all;

entity Fub1 is
	 port(
		 ALUOpExec : in STD_LOGIC;
		 ALUOpExec2 : in STD_LOGIC;
		 ALUOper : out STD_LOGIC_VECTOR(1 downto 0)
	     );
end Fub1;

--}} End of automatically maintained section

architecture Fub1 of Fub1 is
begin
	 ALUOper <= ALUOpExec &  ALUOpExec2;
	 -- enter your statements here --

end Fub1;
