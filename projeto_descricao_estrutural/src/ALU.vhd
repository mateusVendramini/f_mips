-------------------------------------------------------------------------------
--
-- Title       : ALU
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\ALU.vhd
-- Generated   : Sat Jun 29 16:09:44 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ALU} architecture {ALU}}

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ALU is
	 port(
		 RegAEx : in STD_LOGIC_VECTOR(31 downto 0);
		 ULA_in2 : in STD_LOGIC_VECTOR(31 downto 0);
		 ALUSel : in STD_LOGIC_VECTOR(2 downto 0);
		 operand : in STD_LOGIC_VECTOR(10 downto 6);
		 ALU_Result : out STD_LOGIC_VECTOR(31 downto 0);
		 Zero : out STD_LOGIC
	     );
end ALU;

--}} End of automatically maintained section

architecture ALU of ALU is
begin

alu_process:
process(RegAEx,ULA_in2, ALUSel, operand)
begin 
	if (RegAEx = ULA_in2) then
		Zero <= '1' after 5 ns;
	else
		Zero <= '0' after 5 ns;
	end if;
	
	case ALUSel is 	
		when "000" => --sub
			ALU_Result <= std_logic_vector(signed(RegAEx) - signed(ULA_in2)) after 1 ns;
		when "001" => --  add  
			ALU_Result <= std_logic_vector(signed(RegAEx) + signed(ULA_in2)) after 1 ns;
		when "010" =>  --sll
			ALU_Result <= std_logic_vector(shift_left(signed(RegAEx), to_integer(unsigned(operand)))) after 1 ns; 
		when "011" => --slt
		if signed(RegAEx) <  signed(ULA_in2) then
			ALU_Result <= x"00000001" after 1 ns;
		else 
			ALU_Result <= x"00000000" after 1 ns;
		end if;
		
		when "100" => --addu 
			ALU_Result <= std_logic_vector(unsigned(RegAEx) + unsigned(ULA_in2)) after 1 ns; 
		when "101" =>  
			ALU_Result <= RegAEx after 1 ns;
		when "110" =>
			ALU_Result <= RegAEx after 1 ns;
		when "111" =>			 
		ALU_Result <= RegAEx after 1 ns;
		when others =>
	end case;
		
		
end process alu_process;


end ALU;
