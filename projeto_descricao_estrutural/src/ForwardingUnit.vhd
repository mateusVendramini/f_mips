-------------------------------------------------------------------------------
--
-- Title       : FowardingUnit
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\mips\f_mips\projeto_descricao_estrutural\src\FowardingUnit.vhd
-- Generated   : Mon Jul  1 12:34:45 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {FowardingUnit} architecture {FowardingUnit}}

library IEEE;
use IEEE.std_logic_1164.all;

entity ForwardingUnit is
	 port(
		 RegAExLido : in STD_LOGIC_VECTOR(31 downto 0);
		 RegBEXLido : in STD_LOGIC_VECTOR(31 downto 0);
		 RegANum : in STD_LOGIC_VECTOR(4 downto 0);
		 RegBNum : in STD_LOGIC_VECTOR(4 downto 0);
		 RegWriteMem : in STD_LOGIC;
		 RegDstMem : in STD_LOGIC_VECTOR(4 downto 0);
		 RegWriteWB : in STD_LOGIC;
		 AdressOut : in STD_LOGIC_VECTOR(31 downto 0);
		 WriteRegister : in STD_LOGIC_VECTOR(4 downto 0);
		 WriteDataBack : in STD_LOGIC_VECTOR(31 downto 0);
		 RegAEx : out STD_LOGIC_VECTOR(31 downto 0);
		 RegBEX : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end ForwardingUnit;

--}} End of automatically maintained section

architecture ForwardingUnit of ForwardingUnit is
begin
foward_RA_PROCESS:
process(RegAExLido, RegANum, RegWriteMem, RegDstMem, RegWriteWB, AdressOut, WriteRegister, WriteDataBack)
begin
	if RegANum = RegDstMem and 	RegWriteMem = '1' then 
		RegAEx <= AdressOut;
	
	elsif RegANum = WriteRegister and 	RegWriteWB = '1' then
		RegAEx <= WriteDataBack;
		
	else 	--no foward detected
		RegAEx <= RegAExLido;
	end if;
	--end if;
	
	
end process foward_RA_PROCESS; 

foward_RB_PROCESS:
process(RegBExLido, RegBNum, RegWriteMem, RegDstMem, RegWriteWB, AdressOut, WriteRegister, WriteDataBack)
begin
	if RegBNum = RegDstMem and 	RegWriteMem = '1' then 
		RegBEx <= AdressOut;
	
	elsif RegBNum = WriteRegister and 	RegWriteWB = '1' then
		RegBEx <= WriteDataBack;
		
	else 	--no foward detected
		RegBEx <= RegBExLido;
	end if;
	--end if;
	
	
end process foward_RB_PROCESS;

	 -- enter your statements here --

end ForwardingUnit;