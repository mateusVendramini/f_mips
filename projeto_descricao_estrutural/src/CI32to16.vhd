-------------------------------------------------------------------------------
--
-- Title       : CI32to16
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\CI32to16.vhd
-- Generated   : Tue Jul  2 15:38:18 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {CI32to16} architecture {CI32to16}}

library IEEE;
use IEEE.std_logic_1164.all;

entity CI32to16 is
	 port(
		 EnderDados : in STD_LOGIC_VECTOR(31 downto 0);
		 Ender : out STD_LOGIC_VECTOR(15 downto 0)
	     );
end CI32to16;

--}} End of automatically maintained section

architecture CI32to16 of CI32to16 is
begin
	Ender <=  "00" & EnderDados(15 downto 2);

	 -- enter your statements here --

end CI32to16;
