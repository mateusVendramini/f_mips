library IEEE;
use IEEE.std_logic_1164.all;

entity MuxPC is
	 port(
		 NPC_add : in STD_LOGIC_VECTOR(31 downto 0);
		 RegAEx : in STD_LOGIC_VECTOR(31 downto 0);
		 JumpRegSel : in STD_LOGIC;
		 NPC_Br_JR : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end MuxPC;

--}} End of automatically maintained section

architecture MuxPC of MuxPC is
begin
	
TESTE:	
process (NPC_add, RegAEx, JumpRegSel)  
	begin
		Case JumpRegSel is
		when '0' => NPC_Br_JR <= NPC_add after 1 ns;
		when '1' => NPC_Br_JR <= RegAEx after 1 ns;
		when others => NPC_Br_JR <= (others => 'X');
	end case;
end process TESTE;

end MuxPC;

