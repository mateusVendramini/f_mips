-------------------------------------------------------------------------------
--
-- Title       : CkDivider
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\CkDivider.vhd
-- Generated   : Fri Jun 28 19:45:26 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {CkDivider} architecture {CkDivider}}

library IEEE;
use IEEE.std_logic_1164.all;

entity CkDivider is
	 port(
		 ClockControl : in STD_LOGIC;
		 ck2 : out STD_LOGIC;
		 ck1 : out STD_LOGIC;  
		 ck3 : out STD_logic;
		 ck4 : out STD_LOGIC; 
		 ck6 : out STD_LOGIC;
		 ck7 : out STD_LOGIC;
		 Clock : out STD_LOGIC
	     );
end CkDivider;

--}} End of automatically maintained section

architecture CkDivider of CkDivider is
begin 
	ck7 <=    ClockControl;
	ck6 <=    ClockControl;
	ck4 <=    ClockControl;
	ck3 <=    ClockControl;
	ck2 <=    ClockControl;
	ck1 <=    ClockControl;
	Clock <=  ClockControl;

end CkDivider;
