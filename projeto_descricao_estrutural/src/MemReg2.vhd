-------------------------------------------------------------------------------
--
-- Title       : MemReg2
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\MemReg2.vhd
-- Generated   : Sat Jun 29 12:44:39 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {MemReg2} architecture {MemReg2}}

library IEEE;
use IEEE.std_logic_1164.all; 
use ieee.numeric_std.all;

entity MemReg2 is
	 port(
		 RIout : in STD_LOGIC_VECTOR(20 downto 16);	 --Ent2 
		 WriteRegister : in STD_LOGIC_VECTOR(4 downto 0);
		 RIout_1 : in STD_LOGIC_VECTOR(25 downto 21); --Ent1
		 WriteDataBack : in STD_LOGIC_VECTOR(31 downto 0);
		 RegWriteWB : in STD_LOGIC;
		 ck3 : in STD_LOGIC;
		 RegA : out STD_LOGIC_VECTOR(31 downto 0);
		 RegB : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end MemReg2;

--}} End of automatically maintained section

architecture MemReg2 of MemReg2 is	 

type ram_type is array (0 to 2**5 - 1)
        of std_logic_vector (32 - 1 downto 0);
signal ram: ram_type := (others => (others => '0'));  

---- Signal declarations used on the diagram ----

signal enda_reg : std_logic_vector(5 - 1 downto 0);
signal endb_reg : std_logic_vector(5 - 1 downto 0);
signal endw_reg : std_logic_vector(5 - 1 downto 0);


begin

RegisterMemory :
process (ck3)
begin
	 if (ck3'event and ck3 = '1') then
        if (RegWriteWB = '1' and to_integer(unsigned(WriteRegister)) /= 0) then
           ram(to_integer(unsigned(WriteRegister))) <= WriteDataBack;-- after Twrite;
        end if;
        enda_reg <= RIout_1;
        endb_reg <= RIout;
     end if;
	 -- le na borda de descida
	 if (ck3'event and ck3 = '0') then 
		 ---- User Signal Assignments ----
		RegA <= ram(to_integer(unsigned
								(enda_reg))) after 1 ns; --after Tread;
		RegB <= ram(to_integer(unsigned
								(endb_reg))) after 1 ns; --after Tread;
	end if;	   
end process RegisterMemory;

end MemReg2;
