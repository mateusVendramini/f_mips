library IEEE;
use IEEE.std_logic_1164.all;

entity MuxPC_JI is
	 port(
		 NPC_Br_JR : in STD_LOGIC_VECTOR(31 downto 0);
		 JumpAdress : in STD_LOGIC_VECTOR(31 downto 0);
		 PCJumpExec : in STD_LOGIC;
		 NPCExecFinal : out STD_LOGIC_VECTOR(31 downto 0)
	     );
end MuxPC_JI;

--}} End of automatically maintained section

architecture MuxPC_JI of MuxPC_JI is
begin

TESTE:	
process (NPC_Br_JR, JumpAdress, PCJumpExec)  
	begin
		Case PCJumpExec is
		when '0' => NPCExecFinal <= NPC_Br_JR after 1 ns;
		when '1' => NPCExecFinal <= JumpAdress after 1 ns;
		when others => NPCExecFinal <= (others => 'X');
	end case;
end process TESTE;

end MuxPC_JI;
