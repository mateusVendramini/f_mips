-------------------------------------------------------------------------------
--
-- Title       : ID_EX
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\ID_EX.vhd
-- Generated   : Fri Jun 28 22:04:02 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity ID_EX is
	 port(
		 Extended : in STD_LOGIC_VECTOR(31 downto 0);
		 RegA : in STD_LOGIC_VECTOR(31 downto 0);
		 RegB : in STD_LOGIC_VECTOR(31 downto 0);
		 NPC_ID : in STD_LOGIC_VECTOR(31 downto 0);
		 RIout : in STD_LOGIC_VECTOR(15 downto 11);
		 RIout_2 : in STD_LOGIC_VECTOR(20 downto 16);
		 RegWrite : in STD_LOGIC;
		 MemtoReg : in STD_LOGIC;
		 MemWrite : in STD_LOGIC;
		 MemRead : in STD_LOGIC;
		 Branch : in STD_LOGIC;
		 PCSrc : in STD_LOGIC;
		 ALUSrc : in STD_LOGIC;
		 RegDst : in STD_LOGIC;
		 ALUOp : in STD_LOGIC;
		 bneq : in STD_LOGIC;
		 RIout_1 : in STD_LOGIC_VECTOR(25 downto 0);
		 jal : in STD_LOGIC;
		 ALUOp2 : in STD_LOGIC;
		 CE_ID_EX : in STD_LOGIC;
		 rst_id_ex : in STD_LOGIC;
		 ck4 : in STD_LOGIC;
		 RegAExLido : out STD_LOGIC_VECTOR(31 downto 0);
		 RegBExLido : out STD_LOGIC_VECTOR(31 downto 0);
		 operand : out STD_LOGIC_VECTOR(31 downto 0);
		 RegDst0 : out STD_LOGIC_VECTOR(4 downto 0);
		 RegDst1 : out STD_LOGIC_VECTOR(4 downto 0);
		 NPC_Exec : out STD_LOGIC_VECTOR(31 downto 0);
		 RegWriteExec : out STD_LOGIC;
		 MemtoRegExec : out STD_LOGIC;
		 MemWriteExec : out STD_LOGIC;
		 MemReadExec : out STD_LOGIC;
		 BranchExec : out STD_LOGIC;
		 PCJumpExec : out STD_LOGIC;
		 ALUOpExec : out STD_LOGIC;
		 RegDstExec : out STD_LOGIC;
		 ALUSrcExec : out STD_LOGIC;
		 bneqExec : out STD_LOGIC;
		 JumpOperand : out STD_LOGIC_VECTOR(25 downto 0);
		 jalExec : out STD_LOGIC;
		 ALUOpExec2 : out STD_LOGIC;		
		 RegANum: out STD_LOGIC_VECTOR(4 downto 0);
	     RegBNum: out STD_LOGIC_VECTOR(4 downto 0)
		 );
end ID_EX;

architecture ID_EX of ID_EX is
begin

Registrador:
process (ck4, rst_id_ex)

begin
	if rst_id_ex='1' then	-- 	Reset assíncrono
		RegAExLido (31 downto 0) <=  (others => '0');
 		RegBExLido (31 downto 0) <=   (others => '0');			
 		operand (31 downto 0)  <= (others => '0');		
		RegDst0 (4 downto 0) <= (others => '0');	 	
 		RegDst1  (4 downto 0) <=  (others => '0');	 	
 		NPC_Exec  (31 downto 0) <=   (others => '0');	
 		RegWriteExec <= '0';	
 		MemtoRegExec <= '0';	
 		MemWriteExec <= '0';	
 		MemReadExec  <= '0';	
 		BranchExec   <= '0';		
 		PCJumpExec   <= '0';	
 		ALUOpExec 	  <= '0';	
 		RegDstExec   <= '0';	
 		ALUSrcExec   <= '0';	
 		bneqExec 	  <= '0';	
 		JumpOperand  (25 downto 0) <=   (others => '0');	
 		jalExec 	  <= '0';	
 		ALUOpExec2   <= '0';	
		RegANum	(4 downto 0) <= (others => '0');
	    RegBNum	(4 downto 0) <= (others => '0');
	elsif (ck4'event and ck4='1' and CE_ID_EX='1') then  -- Clock na borda de subida
        RegAExLido	  <= RegA after 1 ns;
        RegBExLido	  <= RegB after 1 ns;
        operand 	  <= Extended after 1 ns;
        RegDst0 	  <= RIout_2 after 1 ns;
        RegDst1  	  <= RIout after 1 ns;
        NPC_Exec  	  <= NPC_ID after 1 ns;
        RegWriteExec  <= RegWrite after 1 ns;
        MemtoRegExec  <= MemtoReg after 1 ns;
        MemWriteExec  <= MemWrite after 1 ns;
        MemReadExec   <= MemRead after 1 ns;
        BranchExec    <= Branch after 1 ns;
        PCJumpExec    <= PCSrc after 1 ns;
        ALUOpExec 	  <= ALUOp after 1 ns;
        RegDstExec    <= RegDst after 1 ns;
        ALUSrcExec    <= ALUSrc after 1 ns;
        bneqExec 	  <= bneq after 1 ns;
        JumpOperand   <= RIout_1 after 1 ns;
        jalExec 	  <= jal after 1 ns;
        ALUOpExec2    <= ALUOp2 after 1 ns;	 
		RegANum 	  <= RIout_1(25 downto 21) after 1 ns;
	    RegBNum 	  <= RIout_1(20 downto 16) after 1 ns;
	end if;
end process Registrador;

end ID_EX;
