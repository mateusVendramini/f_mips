-------------------------------------------------------------------------------
--
-- Title       : MuxRegDst
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : c:\Users\MateusVendramini\projects\mips\f_mips\projeto_descricao_estrutural\src\MuxRegDst.vhd
-- Generated   : Sat Jun 29 16:39:10 2019
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {MuxRegDst} architecture {MuxRegDst}}

library IEEE;
use IEEE.std_logic_1164.all;

entity MuxRegDst is
	 port(
		 RegDst0 : in STD_LOGIC_VECTOR(4 downto 0);
		 RegDst1 : in STD_LOGIC_VECTOR(4 downto 0);
		 RegDstExec : in STD_LOGIC;
		 jalExec : in STD_LOGIC;
		 RegDstOut : out STD_LOGIC_VECTOR(4 downto 0)
	     );
end MuxRegDst;

--}} End of automatically maintained section

architecture MuxRegDst of MuxRegDst is
begin

TESTE:	
process (RegDst0, RegDst1, RegDstExec, jalExec)  
begin
	Case jalExec is
		when '0' =>
			Case RegDstExec is
				when '1' => RegDstOut <= RegDst0;
				when '0' => RegDstOut <= RegDst1;
				when others => RegDstOut <= (others => 'X');
			end case;
		when '1' => RegDstOut <= "11111";
		when others => RegDstOut <= (others => 'X');
	end case;
end process TESTE;

end MuxRegDst;

