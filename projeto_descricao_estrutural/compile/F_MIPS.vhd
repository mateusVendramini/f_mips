-------------------------------------------------------------------------------
--
-- Title       : 
-- Design      : projeto_descricao_estrutural
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\User\OneDrive\Documentos\EPUSP\9� Semestre\OrgArq\datapathv2\f_mips\projeto_descricao_estrutural\compile\F_MIPS.vhd
-- Generated   : Tue Jul  2 16:59:31 2019
-- From        : C:\Users\User\OneDrive\Documentos\EPUSP\9� Semestre\OrgArq\datapathv2\f_mips\projeto_descricao_estrutural\src\F_MIPS.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;

entity F_MIPS is
  port(
       Inter : in STD_LOGIC;
       pronto : in STD_LOGIC
  );
end F_MIPS;

architecture F_MIPS of F_MIPS is

---- Component declarations -----

component CacheDados2
  port (
       Adress : in STD_LOGIC_VECTOR(31 downto 0);
       Clock : in STD_LOGIC;
       WriteData : in STD_LOGIC_VECTOR(31 downto 0);
       enable : in STD_LOGIC;
       rw : in STD_LOGIC;
       ReadData : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component CacheI3
  port (
       Clock : in STD_LOGIC;
       EnderDados : in STD_LOGIC_VECTOR(31 downto 0);
       enablei : in STD_LOGIC;
       rwi : in STD_LOGIC;
       DadosCache : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component F_MipsPipeLine
  port (
       DadosCache : in STD_LOGIC_VECTOR(31 downto 0);
       Inter : in STD_LOGIC;
       ReadData : in STD_LOGIC_VECTOR(31 downto 0);
       Reset : in STD_LOGIC;
       hitDados : in STD_LOGIC;
       hitEnd : in STD_LOGIC;
       pronto : in STD_LOGIC;
       Adress : out STD_LOGIC_VECTOR(31 downto 0);
       Clock : out STD_LOGIC;
       EnderDados : out STD_LOGIC_VECTOR(31 downto 0);
       Read : out STD_LOGIC;
       Write : out STD_LOGIC;
       WriteData : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component SinaisGeradosProc
  port (
       enablei : out STD_LOGIC;
       hitDados : out STD_LOGIC;
       hitEnd : out STD_LOGIC;
       rwi : out STD_LOGIC
  );
end component;

---- Signal declarations used on the diagram ----

signal Clock : STD_LOGIC;
signal enable : STD_LOGIC;
signal enablei : STD_LOGIC;
signal hitDados : STD_LOGIC;
signal hitEnd : STD_LOGIC;
signal Read : STD_LOGIC;
signal Reset : STD_LOGIC;
signal rw : STD_LOGIC;
signal rwi : STD_LOGIC;
signal Write : STD_LOGIC;
signal Adress : STD_LOGIC_VECTOR(31 downto 0);
signal DadosCache : STD_LOGIC_VECTOR(31 downto 0);
signal EnderDados : STD_LOGIC_VECTOR(31 downto 0);
signal ReadData : STD_LOGIC_VECTOR(31 downto 0);
signal WriteData : STD_LOGIC_VECTOR(31 downto 0);

begin

----  Component instantiations  ----

U1 : CacheI3
  port map(
       Clock => Clock,
       DadosCache => DadosCache,
       EnderDados => EnderDados,
       enablei => enablei,
       rwi => rwi
  );

rw <= not(Read) and Write;

enable <= Write or Read;

U4 : SinaisGeradosProc
  port map(
       enablei => enablei,
       hitDados => hitDados,
       hitEnd => hitEnd,
       rwi => rwi
  );

U5 : F_MipsPipeLine
  port map(
       Adress => Adress,
       Clock => Clock,
       DadosCache => DadosCache,
       EnderDados => EnderDados,
       Inter => Inter,
       Read => Read,
       ReadData => ReadData,
       Reset => Reset,
       Write => Write,
       WriteData => WriteData,
       hitDados => hitDados,
       hitEnd => hitEnd,
       pronto => pronto
  );

U6 : CacheDados2
  port map(
       Adress => Adress,
       Clock => Clock,
       ReadData => ReadData,
       WriteData => WriteData,
       enable => enable,
       rw => rw
  );


end F_MIPS;
