-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : projeto_descricao_estrutural
-- Author      : Mateus Vendramini
-- Company     : University of Sao Paulo
--
-------------------------------------------------------------------------------
--
-- File        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\compile\sistema.vhd
-- Generated   : Tue Jul  2 13:08:24 2019
-- From        : C:\VersaoRicardo\f_mips\projeto_descricao_estrutural\src\sistema.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_unsigned.all;

entity sistema is 
end sistema;

architecture sistema of sistema is

---- Component declarations -----

component F_MIPS
  port (
       Inter : in STD_LOGIC;
       pronto : in STD_LOGIC;
       Clock : out STD_LOGIC;
       Ender : out STD_LOGIC_VECTOR(31 downto 0);
       ResetOut : out STD_LOGIC;
       enable : out STD_LOGIC;
       rw : out STD_LOGIC;
       Dados : inout STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component F_Poli
  port (
       Clock : in STD_LOGIC;
       Ender : in STD_LOGIC_VECTOR(31 downto 0);
       ResetOut : in STD_LOGIC;
       enable : in STD_LOGIC;
       rw : in STD_LOGIC;
       Inter : out STD_LOGIC;
       pronto : out STD_LOGIC;
       Dados : inout STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MemoriaPrincipal
  port (
       Clock : in STD_LOGIC;
       Ender : in STD_LOGIC_VECTOR(31 downto 0);
       enable : in STD_LOGIC;
       rw : in STD_LOGIC;
       pronto : out STD_LOGIC;
       Dados : inout STD_LOGIC_VECTOR(31 downto 0)
  );
end component;

---- Signal declarations used on the diagram ----

signal Clock : STD_LOGIC;
signal enable : STD_LOGIC;
signal Inter : STD_LOGIC;
signal pronto : STD_LOGIC;
signal ResetOut : STD_LOGIC;
signal rw : STD_LOGIC;
signal Dados : STD_LOGIC_VECTOR(31 downto 0);
signal Ender : STD_LOGIC_VECTOR(31 downto 0);

begin

----  Component instantiations  ----

U1 : MemoriaPrincipal
  port map(
       Clock => Clock,
       Dados => Dados,
       Ender => Ender,
       enable => enable,
       pronto => pronto,
       rw => rw
  );

U2 : F_MIPS
  port map(
       Clock => Clock,
       Dados => Dados,
       Ender => Ender,
       Inter => Inter,
       ResetOut => ResetOut,
       enable => enable,
       pronto => pronto,
       rw => rw
  );

U4 : F_Poli
  port map(
       Clock => Clock,
       Dados => Dados,
       Ender => Ender,
       Inter => Inter,
       ResetOut => ResetOut,
       enable => enable,
       pronto => pronto,
       rw => rw
  );


end sistema;
