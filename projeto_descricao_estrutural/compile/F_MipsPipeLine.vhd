-------------------------------------------------------------------------------
--
-- Title       : 
-- Design      : projeto_descricao_estrutural
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : C:\Users\User\OneDrive\Documentos\EPUSP\9� Semestre\OrgArq\datapathv2\f_mips\projeto_descricao_estrutural\compile\F_MipsPipeLine.vhd
-- Generated   : Tue Jul  2 21:12:01 2019
-- From        : C:\Users\User\OneDrive\Documentos\EPUSP\9� Semestre\OrgArq\datapathv2\f_mips\projeto_descricao_estrutural\src\F_MipsPipeLine.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;

entity F_MipsPipeLine is
  port(
       Inter : in STD_LOGIC;
       Reset : in STD_LOGIC;
       hitDados : in STD_LOGIC;
       hitEnd : in STD_LOGIC;
       pronto : in STD_LOGIC;
       DadosCache : in STD_LOGIC_VECTOR(31 downto 0);
       ReadData : in STD_LOGIC_VECTOR(31 downto 0);
       Clock : out STD_LOGIC;
       Read : out STD_LOGIC;
       Write : out STD_LOGIC;
       Adress : out STD_LOGIC_VECTOR(31 downto 0);
       EnderDados : out STD_LOGIC_VECTOR(31 downto 0);
       WriteData : out STD_LOGIC_VECTOR(31 downto 0)
  );
end F_MipsPipeLine;

architecture F_MipsPipeLine of F_MipsPipeLine is

---- Component declarations -----

component ADD
  port (
       ExtendedShifted : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_Exec : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_add : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component ALU
  port (
       ALUSel : in STD_LOGIC_VECTOR(2 downto 0);
       RegAEx : in STD_LOGIC_VECTOR(31 downto 0);
       ULA_in2 : in STD_LOGIC_VECTOR(31 downto 0);
       operand : in STD_LOGIC_VECTOR(10 downto 6);
       ALU_Result : out STD_LOGIC_VECTOR(31 downto 0);
       Zero : out STD_LOGIC
  );
end component;
component ALU_Control
  port (
       ALUOper : in STD_LOGIC_VECTOR(1 downto 0);
       operand : in STD_LOGIC_VECTOR(5 downto 0);
       ALUSel : out STD_LOGIC_VECTOR(2 downto 0);
       JumpRegSel : out STD_LOGIC
  );
end component;
component CalculaJump
  port (
       JumpOperand : in STD_LOGIC_VECTOR(25 downto 0);
       NPC_Exec : in STD_LOGIC_VECTOR(31 downto 0);
       JumpAdress : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component CkDivider
  port (
       ClockControl : in STD_LOGIC;
       Clock : out STD_LOGIC;
       ck1 : out STD_LOGIC;
       ck2 : out STD_LOGIC;
       ck3 : out STD_LOGIC;
       ck4 : out STD_LOGIC;
       ck6 : out STD_LOGIC;
       ck7 : out STD_LOGIC
  );
end component;
component Concatena
  port (
       ALUOpExec : in STD_LOGIC;
       ALUOpExec2 : in STD_LOGIC;
       ALUOper : out STD_LOGIC_VECTOR(1 downto 0)
  );
end component;
component Control
  port (
       RIout : in STD_LOGIC_VECTOR(31 downto 26);
       Reset : in STD_LOGIC;
       ALUOp : out STD_LOGIC;
       ALUOp2 : out STD_LOGIC;
       ALUSrc : out STD_LOGIC;
       Branch : out STD_LOGIC;
       ClockControl : out STD_LOGIC;
       MemRead : out STD_LOGIC;
       MemWrite : out STD_LOGIC;
       MemtoReg : out STD_LOGIC;
       PCSrc : out STD_LOGIC;
       RegDst : out STD_LOGIC;
       RegWrite : out STD_LOGIC;
       Rst_regs : out STD_LOGIC;
       bneq : out STD_LOGIC;
       jal : out STD_LOGIC
  );
end component;
component EX_MEM
  port (
       BranchExec : in STD_LOGIC;
       CE_EX_MEM : in STD_LOGIC;
       ExecResult : in STD_LOGIC_VECTOR(31 downto 0);
       MemReadExec : in STD_LOGIC;
       MemWriteExec : in STD_LOGIC;
       MemtoRegExec : in STD_LOGIC;
       NPCExecFinal : in STD_LOGIC_VECTOR(31 downto 0);
       PCJumpJRExec : in STD_LOGIC;
       RegBEX : in STD_LOGIC_VECTOR(31 downto 0);
       RegDstOut : in STD_LOGIC_VECTOR(4 downto 0);
       RegWriteExec : in STD_LOGIC;
       Zero : in STD_LOGIC;
       bneqExec : in STD_LOGIC;
       ck6 : in STD_LOGIC;
       rst_ex_mem : in STD_LOGIC;
       AdressOut : out STD_LOGIC_VECTOR(31 downto 0);
       Branch_Mem : out STD_LOGIC;
       MemtoRegMem : out STD_LOGIC;
       NPCJ : out STD_LOGIC_VECTOR(31 downto 0);
       PCJump_Mem : out STD_LOGIC;
       Read : out STD_LOGIC;
       RegDstMem : out STD_LOGIC_VECTOR(4 downto 0);
       RegWriteMem : out STD_LOGIC;
       Write : out STD_LOGIC;
       WriteData : out STD_LOGIC_VECTOR(31 downto 0);
       Zero_Mem : out STD_LOGIC;
       bneq_Mem : out STD_LOGIC
  );
end component;
component ForwardingUnit
  port (
       AdressOut : in STD_LOGIC_VECTOR(31 downto 0);
       RegAExLido : in STD_LOGIC_VECTOR(31 downto 0);
       RegANum : in STD_LOGIC_VECTOR(4 downto 0);
       RegBExLido : in STD_LOGIC_VECTOR(31 downto 0);
       RegBNum : in STD_LOGIC_VECTOR(4 downto 0);
       RegDstMem : in STD_LOGIC_VECTOR(4 downto 0);
       RegWriteMem : in STD_LOGIC;
       RegWriteWB : in STD_LOGIC;
       WriteDataBack : in STD_LOGIC_VECTOR(31 downto 0);
       WriteRegister : in STD_LOGIC_VECTOR(4 downto 0);
       RegAEx : out STD_LOGIC_VECTOR(31 downto 0);
       RegBEX : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component ID_EX
  port (
       ALUOp : in STD_LOGIC;
       ALUOp2 : in STD_LOGIC;
       ALUSrc : in STD_LOGIC;
       Branch : in STD_LOGIC;
       CE_ID_EX : in STD_LOGIC;
       Extended : in STD_LOGIC_VECTOR(31 downto 0);
       MemRead : in STD_LOGIC;
       MemWrite : in STD_LOGIC;
       MemtoReg : in STD_LOGIC;
       NPC_ID : in STD_LOGIC_VECTOR(31 downto 0);
       PCSrc : in STD_LOGIC;
       RIout : in STD_LOGIC_VECTOR(15 downto 11);
       RIout_1 : in STD_LOGIC_VECTOR(25 downto 0);
       RIout_2 : in STD_LOGIC_VECTOR(20 downto 16);
       RegA : in STD_LOGIC_VECTOR(31 downto 0);
       RegB : in STD_LOGIC_VECTOR(31 downto 0);
       RegDst : in STD_LOGIC;
       RegWrite : in STD_LOGIC;
       bneq : in STD_LOGIC;
       ck4 : in STD_LOGIC;
       jal : in STD_LOGIC;
       rst_id_ex : in STD_LOGIC;
       ALUOpExec : out STD_LOGIC;
       ALUOpExec2 : out STD_LOGIC;
       ALUSrcExec : out STD_LOGIC;
       BranchExec : out STD_LOGIC;
       JumpOperand : out STD_LOGIC_VECTOR(25 downto 0);
       MemReadExec : out STD_LOGIC;
       MemWriteExec : out STD_LOGIC;
       MemtoRegExec : out STD_LOGIC;
       NPC_Exec : out STD_LOGIC_VECTOR(31 downto 0);
       PCJumpExec : out STD_LOGIC;
       RegAExLido : out STD_LOGIC_VECTOR(31 downto 0);
       RegANum : out STD_LOGIC_VECTOR(4 downto 0);
       RegBExLido : out STD_LOGIC_VECTOR(31 downto 0);
       RegBNum : out STD_LOGIC_VECTOR(4 downto 0);
       RegDst0 : out STD_LOGIC_VECTOR(4 downto 0);
       RegDst1 : out STD_LOGIC_VECTOR(4 downto 0);
       RegDstExec : out STD_LOGIC;
       RegWriteExec : out STD_LOGIC;
       bneqExec : out STD_LOGIC;
       jalExec : out STD_LOGIC;
       operand : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component IF_ID
  port (
       CE_RI : in STD_LOGIC;
       NPC_IF : in STD_LOGIC_VECTOR(31 downto 0);
       RI : in STD_LOGIC_VECTOR(31 downto 0);
       ck1 : in STD_LOGIC;
       rst_if_id : in STD_LOGIC;
       NPC_ID : out STD_LOGIC_VECTOR(31 downto 0);
       RIout : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MemReg2
  port (
       RIout : in STD_LOGIC_VECTOR(20 downto 16);
       RIout_1 : in STD_LOGIC_VECTOR(25 downto 21);
       RegWriteWB : in STD_LOGIC;
       WriteDataBack : in STD_LOGIC_VECTOR(31 downto 0);
       WriteRegister : in STD_LOGIC_VECTOR(4 downto 0);
       ck3 : in STD_LOGIC;
       RegA : out STD_LOGIC_VECTOR(31 downto 0);
       RegB : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component Mem_WB
  port (
       AdressOut : in STD_LOGIC_VECTOR(31 downto 0);
       CE_MEM_WB : in STD_LOGIC;
       MemtoRegMem : in STD_LOGIC;
       ReadData : in STD_LOGIC_VECTOR(31 downto 0);
       RegDstMem : in STD_LOGIC_VECTOR(4 downto 0);
       RegWriteMem : in STD_LOGIC;
       ck7 : in STD_LOGIC;
       rst_mem_wb : in STD_LOGIC;
       ALUData : out STD_LOGIC_VECTOR(31 downto 0);
       MemtoRegWB : out STD_LOGIC;
       ReadDataBack : out STD_LOGIC_VECTOR(31 downto 0);
       RegWriteWB : out STD_LOGIC;
       WriteRegister : out STD_LOGIC_VECTOR(4 downto 0)
  );
end component;
component Mux
  port (
       ALUData : in STD_LOGIC_VECTOR(31 downto 0);
       MemtoRegWB : in STD_LOGIC;
       ReadDataBack : in STD_LOGIC_VECTOR(31 downto 0);
       WriteDataBack : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MuxALU
  port (
       ALUSrcExec : in STD_LOGIC;
       RegBEX : in STD_LOGIC_VECTOR(31 downto 0);
       operand : in STD_LOGIC_VECTOR(31 downto 0);
       ULA_in2 : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MuxJumpRegister
  port (
       ALU_Result : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_Exec : in STD_LOGIC_VECTOR(31 downto 0);
       jalExec : in STD_LOGIC;
       ExecResult : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MuxPC
  port (
       JumpRegSel : in STD_LOGIC;
       NPC_add : in STD_LOGIC_VECTOR(31 downto 0);
       RegAEx : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_Br_JR : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MuxPC2
  port (
       NPCJ : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_IF : in STD_LOGIC_VECTOR(31 downto 0);
       PCSrcOut : in STD_LOGIC;
       NPC : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MuxPC_JI
  port (
       JumpAdress : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_Br_JR : in STD_LOGIC_VECTOR(31 downto 0);
       PCJumpExec : in STD_LOGIC;
       NPCExecFinal : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component MuxRegDst
  port (
       RegDst0 : in STD_LOGIC_VECTOR(4 downto 0);
       RegDst1 : in STD_LOGIC_VECTOR(4 downto 0);
       RegDstExec : in STD_LOGIC;
       jalExec : in STD_LOGIC;
       RegDstOut : out STD_LOGIC_VECTOR(4 downto 0)
  );
end component;
component PCReg
  port (
       CE_RI : in STD_LOGIC;
       NPC : in STD_LOGIC_VECTOR(31 downto 0);
       Rst_regs : in STD_LOGIC;
       ck2 : in STD_LOGIC;
       PC : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component SignExtend
  port (
       RIout : in STD_LOGIC_VECTOR(15 downto 0);
       Extended : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component SL2
  port (
       operand : in STD_LOGIC_VECTOR(31 downto 0);
       ExtendedShifted : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component Soma4
  port (
       PC : in STD_LOGIC_VECTOR(31 downto 0);
       NPC_IF : out STD_LOGIC_VECTOR(31 downto 0)
  );
end component;
component Stall_Control
  port (
       PCSrcOut : in STD_LOGIC;
       Rst_regs : in STD_LOGIC;
       hitDados : in STD_LOGIC;
       hitEnd : in STD_LOGIC;
       CE_EX_MEM : out STD_LOGIC;
       CE_ID_EX : out STD_LOGIC;
       CE_MEM_WB : out STD_LOGIC;
       CE_RI : out STD_LOGIC;
       rst_ex_mem : out STD_LOGIC;
       rst_id_ex : out STD_LOGIC;
       rst_if_id : out STD_LOGIC;
       rst_mem_wb : out STD_LOGIC
  );
end component;

---- Signal declarations used on the diagram ----

signal ALUOp : STD_LOGIC;
signal ALUOp2 : STD_LOGIC;
signal ALUOpExec : STD_LOGIC;
signal ALUOpExec2 : STD_LOGIC;
signal ALUSrc : STD_LOGIC;
signal ALUSrcExec : STD_LOGIC;
signal bneq : STD_LOGIC;
signal bneqExec : STD_LOGIC;
signal bneq_Mem : STD_LOGIC;
signal Branch : STD_LOGIC;
signal BranchExec : STD_LOGIC;
signal Branch_Mem : STD_LOGIC;
signal CE_EX_MEM : STD_LOGIC;
signal CE_ID_EX : STD_LOGIC;
signal CE_MEM_WB : STD_LOGIC;
signal CE_RI : STD_LOGIC;
signal ck1 : STD_LOGIC;
signal ck2 : STD_LOGIC;
signal ck3 : STD_LOGIC;
signal ck4 : STD_LOGIC;
signal ck6 : STD_LOGIC;
signal ck7 : STD_LOGIC;
signal ClockControl : STD_LOGIC;
signal jal : STD_LOGIC;
signal jalExec : STD_LOGIC;
signal JumpRegSel : STD_LOGIC;
signal MemRead : STD_LOGIC;
signal MemReadExec : STD_LOGIC;
signal MemtoReg : STD_LOGIC;
signal MemtoRegExec : STD_LOGIC;
signal MemtoRegMem : STD_LOGIC;
signal MemtoRegWB : STD_LOGIC;
signal MemWrite : STD_LOGIC;
signal MemWriteExec : STD_LOGIC;
signal NET22372 : STD_LOGIC;
signal NET22394 : STD_LOGIC;
signal NET5169 : STD_LOGIC;
signal PCJumpExec : STD_LOGIC;
signal PCJumpJRExec : STD_LOGIC;
signal PCJump_Mem : STD_LOGIC;
signal PCSrc : STD_LOGIC;
signal PCSrcOut : STD_LOGIC;
signal RegDst : STD_LOGIC;
signal RegDstExec : STD_LOGIC;
signal RegWrite : STD_LOGIC;
signal RegWriteExec : STD_LOGIC;
signal RegWriteMem : STD_LOGIC;
signal RegWriteWB : STD_LOGIC;
signal rst_ex_mem : STD_LOGIC;
signal rst_id_ex : STD_LOGIC;
signal rst_if_id : STD_LOGIC;
signal rst_mem_wb : STD_LOGIC;
signal Rst_regs : STD_LOGIC;
signal Zero : STD_LOGIC;
signal Zero_Mem : STD_LOGIC;
signal AdressOut : STD_LOGIC_VECTOR(31 downto 0);
signal ALUData : STD_LOGIC_VECTOR(31 downto 0);
signal ALUOper : STD_LOGIC_VECTOR(1 downto 0);
signal ALUSel : STD_LOGIC_VECTOR(2 downto 0);
signal ALU_Result : STD_LOGIC_VECTOR(31 downto 0);
signal ExecResult : STD_LOGIC_VECTOR(31 downto 0);
signal Extended : STD_LOGIC_VECTOR(31 downto 0);
signal ExtendedShifted : STD_LOGIC_VECTOR(31 downto 0);
signal JumpAdress : STD_LOGIC_VECTOR(31 downto 0);
signal JumpOperand : STD_LOGIC_VECTOR(25 downto 0);
signal NPC : STD_LOGIC_VECTOR(31 downto 0);
signal NPCExecFinal : STD_LOGIC_VECTOR(31 downto 0);
signal NPCJ : STD_LOGIC_VECTOR(31 downto 0);
signal NPC_add : STD_LOGIC_VECTOR(31 downto 0);
signal NPC_Br_JR : STD_LOGIC_VECTOR(31 downto 0);
signal NPC_Exec : STD_LOGIC_VECTOR(31 downto 0);
signal NPC_ID : STD_LOGIC_VECTOR(31 downto 0);
signal NPC_IF : STD_LOGIC_VECTOR(31 downto 0);
signal operand : STD_LOGIC_VECTOR(31 downto 0);
signal PC : STD_LOGIC_VECTOR(31 downto 0);
signal ReadDataBack : STD_LOGIC_VECTOR(31 downto 0);
signal RegA : STD_LOGIC_VECTOR(31 downto 0);
signal RegAEx : STD_LOGIC_VECTOR(31 downto 0);
signal RegAExLido : STD_LOGIC_VECTOR(31 downto 0);
signal RegANum : STD_LOGIC_VECTOR(4 downto 0);
signal RegB : STD_LOGIC_VECTOR(31 downto 0);
signal RegBEX : STD_LOGIC_VECTOR(31 downto 0);
signal RegBExLido : STD_LOGIC_VECTOR(31 downto 0);
signal RegBNum : STD_LOGIC_VECTOR(4 downto 0);
signal RegDst0 : STD_LOGIC_VECTOR(4 downto 0);
signal RegDst1 : STD_LOGIC_VECTOR(4 downto 0);
signal RegDstMem : STD_LOGIC_VECTOR(4 downto 0);
signal RegDstOut : STD_LOGIC_VECTOR(4 downto 0);
signal RI : STD_LOGIC_VECTOR(31 downto 0);
signal RIout : STD_LOGIC_VECTOR(31 downto 0);
signal ULA_in2 : STD_LOGIC_VECTOR(31 downto 0);
signal WriteDataBack : STD_LOGIC_VECTOR(31 downto 0);
signal WriteRegister : STD_LOGIC_VECTOR(4 downto 0);

begin

----  Component instantiations  ----

U10 : ALU
  port map(
       ALUSel => ALUSel,
       ALU_Result => ALU_Result,
       operand(6) => operand(6),
       operand(7) => operand(7),
       operand(8) => operand(8),
       operand(9) => operand(9),
       operand(10) => operand(10),
       RegAEx => RegAEx,
       ULA_in2 => ULA_in2,
       Zero => Zero
  );

U11 : SL2
  port map(
       ExtendedShifted => ExtendedShifted,
       operand => operand
  );

U12 : MuxRegDst
  port map(
       RegDst0 => RegDst0,
       RegDst1 => RegDst1,
       RegDstExec => RegDstExec,
       RegDstOut => RegDstOut,
       jalExec => jalExec
  );

U13 : ALU_Control
  port map(
       ALUOper => ALUOper,
       ALUSel => ALUSel,
       JumpRegSel => JumpRegSel,
       operand(0) => operand(0),
       operand(1) => operand(1),
       operand(2) => operand(2),
       operand(3) => operand(3),
       operand(4) => operand(4),
       operand(5) => operand(5)
  );

U14 : ADD
  port map(
       ExtendedShifted => ExtendedShifted,
       NPC_Exec => NPC_Exec,
       NPC_add => NPC_add
  );

U15 : MuxPC
  port map(
       JumpRegSel => JumpRegSel,
       NPC_Br_JR => NPC_Br_JR,
       NPC_add => NPC_add,
       RegAEx => RegAEx
  );

U16 : EX_MEM
  port map(
       AdressOut => AdressOut,
       BranchExec => BranchExec,
       Branch_Mem => Branch_Mem,
       CE_EX_MEM => CE_EX_MEM,
       ExecResult => ExecResult,
       MemReadExec => MemReadExec,
       MemWriteExec => MemWriteExec,
       MemtoRegExec => MemtoRegExec,
       MemtoRegMem => MemtoRegMem,
       NPCExecFinal => NPCExecFinal,
       NPCJ => NPCJ,
       PCJumpJRExec => PCJumpJRExec,
       PCJump_Mem => PCJump_Mem,
       Read => Read,
       RegBEX => RegBEX,
       RegDstMem => RegDstMem,
       RegDstOut => RegDstOut,
       RegWriteExec => RegWriteExec,
       RegWriteMem => RegWriteMem,
       Write => Write,
       WriteData => WriteData,
       Zero => Zero,
       Zero_Mem => Zero_Mem,
       bneqExec => bneqExec,
       bneq_Mem => bneq_Mem,
       ck6 => ck6,
       rst_ex_mem => rst_ex_mem
  );

NET5169 <= Zero_Mem and Branch_Mem;

U18 : Mem_WB
  port map(
       ALUData => ALUData,
       AdressOut => AdressOut,
       CE_MEM_WB => CE_MEM_WB,
       MemtoRegMem => MemtoRegMem,
       MemtoRegWB => MemtoRegWB,
       ReadData => ReadData,
       ReadDataBack => ReadDataBack,
       RegDstMem => RegDstMem,
       RegWriteMem => RegWriteMem,
       RegWriteWB => RegWriteWB,
       WriteRegister => WriteRegister,
       ck7 => ck7,
       rst_mem_wb => rst_mem_wb
  );

U19 : Mux
  port map(
       ALUData => ALUData,
       MemtoRegWB => MemtoRegWB,
       ReadDataBack => ReadDataBack,
       WriteDataBack => WriteDataBack
  );

U2 : PCReg
  port map(
       CE_RI => CE_RI,
       NPC => NPC,
       PC => PC,
       Rst_regs => Rst_regs,
       ck2 => ck2
  );

U20 : Control
  port map(
       ALUOp => ALUOp,
       ALUOp2 => ALUOp2,
       ALUSrc => ALUSrc,
       Branch => Branch,
       ClockControl => ClockControl,
       MemRead => MemRead,
       MemWrite => MemWrite,
       MemtoReg => MemtoReg,
       PCSrc => PCSrc,
       RIout(26) => RIout(26),
       RIout(27) => RIout(27),
       RIout(28) => RIout(28),
       RIout(29) => RIout(29),
       RIout(30) => RIout(30),
       RIout(31) => RIout(31),
       RegDst => RegDst,
       RegWrite => RegWrite,
       Reset => Reset,
       Rst_regs => Rst_regs,
       bneq => bneq,
       jal => jal
  );

NET22394 <= NET5169 xor NET22372;

U22 : CkDivider
  port map(
       Clock => Clock,
       ClockControl => ClockControl,
       ck1 => ck1,
       ck2 => ck2,
       ck3 => ck3,
       ck4 => ck4,
       ck6 => ck6,
       ck7 => ck7
  );

PCSrcOut <= PCJump_Mem or NET22394;

U24 : MuxPC_JI
  port map(
       JumpAdress => JumpAdress,
       NPCExecFinal => NPCExecFinal,
       NPC_Br_JR => NPC_Br_JR,
       PCJumpExec => PCJumpExec
  );

U25 : CalculaJump
  port map(
       JumpAdress => JumpAdress,
       JumpOperand => JumpOperand,
       NPC_Exec => NPC_Exec
  );

U26 : MuxJumpRegister
  port map(
       ALU_Result => ALU_Result,
       ExecResult => ExecResult,
       NPC_Exec => NPC_Exec,
       jalExec => jalExec
  );

U27 : Concatena
  port map(
       ALUOpExec => ALUOpExec,
       ALUOpExec2 => ALUOpExec2,
       ALUOper => ALUOper
  );

U28 : ForwardingUnit
  port map(
       AdressOut => AdressOut,
       RegAEx => RegAEx,
       RegAExLido => RegAExLido,
       RegANum => RegANum,
       RegBEX => RegBEX,
       RegBExLido => RegBExLido,
       RegBNum => RegBNum,
       RegDstMem => RegDstMem,
       RegWriteMem => RegWriteMem,
       RegWriteWB => RegWriteWB,
       WriteDataBack => WriteDataBack,
       WriteRegister => WriteRegister
  );

NET22372 <= not(Zero_Mem) and bneq_Mem;

U3 : MuxPC2
  port map(
       NPC => NPC,
       NPCJ => NPCJ,
       NPC_IF => NPC_IF,
       PCSrcOut => PCSrcOut
  );

PCJumpJRExec <= JumpRegSel or PCJumpExec;

U33 : Stall_Control
  port map(
       CE_EX_MEM => CE_EX_MEM,
       CE_ID_EX => CE_ID_EX,
       CE_MEM_WB => CE_MEM_WB,
       CE_RI => CE_RI,
       PCSrcOut => PCSrcOut,
       Rst_regs => Rst_regs,
       hitDados => hitDados,
       hitEnd => hitEnd,
       rst_ex_mem => rst_ex_mem,
       rst_id_ex => rst_id_ex,
       rst_if_id => rst_if_id,
       rst_mem_wb => rst_mem_wb
  );

U4 : Soma4
  port map(
       NPC_IF => NPC_IF,
       PC => PC
  );

U5 : IF_ID
  port map(
       CE_RI => CE_RI,
       NPC_ID => NPC_ID,
       NPC_IF => NPC_IF,
       RI => RI,
       RIout => RIout,
       ck1 => ck1,
       rst_if_id => rst_if_id
  );

U6 : MemReg2
  port map(
       RIout(16) => RIout(16),
       RIout(17) => RIout(17),
       RIout(18) => RIout(18),
       RIout(19) => RIout(19),
       RIout(20) => RIout(20),
       RIout_1(21) => RIout(21),
       RIout_1(22) => RIout(22),
       RIout_1(23) => RIout(23),
       RIout_1(24) => RIout(24),
       RIout_1(25) => RIout(25),
       RegA => RegA,
       RegB => RegB,
       RegWriteWB => RegWriteWB,
       WriteDataBack => WriteDataBack,
       WriteRegister => WriteRegister,
       ck3 => ck3
  );

U7 : ID_EX
  port map(
       ALUOp => ALUOp,
       ALUOp2 => ALUOp2,
       ALUOpExec => ALUOpExec,
       ALUOpExec2 => ALUOpExec2,
       ALUSrc => ALUSrc,
       ALUSrcExec => ALUSrcExec,
       Branch => Branch,
       BranchExec => BranchExec,
       CE_ID_EX => CE_ID_EX,
       Extended => Extended,
       JumpOperand => JumpOperand,
       MemRead => MemRead,
       MemReadExec => MemReadExec,
       MemWrite => MemWrite,
       MemWriteExec => MemWriteExec,
       MemtoReg => MemtoReg,
       MemtoRegExec => MemtoRegExec,
       NPC_Exec => NPC_Exec,
       NPC_ID => NPC_ID,
       PCJumpExec => PCJumpExec,
       PCSrc => PCSrc,
       RIout(11) => RIout(11),
       RIout(12) => RIout(12),
       RIout(13) => RIout(13),
       RIout(14) => RIout(14),
       RIout(15) => RIout(15),
       RIout_1(0) => RIout(0),
       RIout_1(1) => RIout(1),
       RIout_1(2) => RIout(2),
       RIout_1(3) => RIout(3),
       RIout_1(4) => RIout(4),
       RIout_1(5) => RIout(5),
       RIout_1(6) => RIout(6),
       RIout_1(7) => RIout(7),
       RIout_1(8) => RIout(8),
       RIout_1(9) => RIout(9),
       RIout_1(10) => RIout(10),
       RIout_1(11) => RIout(11),
       RIout_1(12) => RIout(12),
       RIout_1(13) => RIout(13),
       RIout_1(14) => RIout(14),
       RIout_1(15) => RIout(15),
       RIout_1(16) => RIout(16),
       RIout_1(17) => RIout(17),
       RIout_1(18) => RIout(18),
       RIout_1(19) => RIout(19),
       RIout_1(20) => RIout(20),
       RIout_1(21) => RIout(21),
       RIout_1(22) => RIout(22),
       RIout_1(23) => RIout(23),
       RIout_1(24) => RIout(24),
       RIout_1(25) => RIout(25),
       RIout_2(16) => RIout(16),
       RIout_2(17) => RIout(17),
       RIout_2(18) => RIout(18),
       RIout_2(19) => RIout(19),
       RIout_2(20) => RIout(20),
       RegA => RegA,
       RegAExLido => RegAExLido,
       RegANum => RegANum,
       RegB => RegB,
       RegBExLido => RegBExLido,
       RegBNum => RegBNum,
       RegDst => RegDst,
       RegDst0 => RegDst0,
       RegDst1 => RegDst1,
       RegDstExec => RegDstExec,
       RegWrite => RegWrite,
       RegWriteExec => RegWriteExec,
       bneq => bneq,
       bneqExec => bneqExec,
       ck4 => ck4,
       jal => jal,
       jalExec => jalExec,
       operand => operand,
       rst_id_ex => rst_id_ex
  );

U8 : SignExtend
  port map(
       Extended => Extended,
       RIout(0) => RIout(0),
       RIout(1) => RIout(1),
       RIout(2) => RIout(2),
       RIout(3) => RIout(3),
       RIout(4) => RIout(4),
       RIout(5) => RIout(5),
       RIout(6) => RIout(6),
       RIout(7) => RIout(7),
       RIout(8) => RIout(8),
       RIout(9) => RIout(9),
       RIout(10) => RIout(10),
       RIout(11) => RIout(11),
       RIout(12) => RIout(12),
       RIout(13) => RIout(13),
       RIout(14) => RIout(14),
       RIout(15) => RIout(15)
  );

U9 : MuxALU
  port map(
       ALUSrcExec => ALUSrcExec,
       RegBEX => RegBEX,
       ULA_in2 => ULA_in2,
       operand => operand
  );


---- Terminal assignment ----

    -- Inputs terminals
	RI <= DadosCache;

    -- Output\buffer terminals
	Adress <= AdressOut;
	EnderDados <= PC;


end F_MipsPipeLine;
