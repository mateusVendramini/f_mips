# multiplicação
.data
N: .word  3
M: .word  4
.text
.align 2
.globl __start
__start:
	lw   $a0, N
	lw   $a1, M
	jal  multiply
	add  $t0, $v0, $zero # Call the Halt system call
	addi $v0, $zero, 10 # Call the Halt system call
	syscall # Halt the program

.globl multiply
.ent multiply
multiply:
	sw   $ra, -4($sp)
	sw   $s0, -8($sp)
	sw   $s1, -12($sp)
	addi $sp, $sp, -16

	# save arguments
	add $s0, $a0, $zero     # N
	add $s1, $a1, $zero 	# M
	addi $t3, $zero, 1

slt_mult:
	beq $s0, $zero, exit

	add  $t2, $s1, $t2
	addi $s0, $s0, -1
	j slt_mult

exit: 
	add  $v0, $t2, $zero
	addi $sp, $sp, 16
	lw   $ra, -4($sp)
	lw   $s0, -8($sp)
	lw   $s1, -12($sp)
	jr   $ra
.end multiply