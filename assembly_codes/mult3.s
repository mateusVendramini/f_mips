# multiplicação de matrizes
# https://lukakerr.github.io/assembly/matrix-multiplication-in-mips
.data
N: .word  2
M: .word  2
P: .word  2
A: .word  1, 2, 3, 4   # { {1, 2}, {3, 4}, {5, 6} }
B: .word  8, 7, 6, 5   # { {8, 7, 6, 5}, {4, 3, 2, 1} }
C: .space 48

.text
.align 2
.globl __start
__start:
	addi $t0, 0x1001
	sll  $t0, $t0, 16
	lw   $a0, 0($t0)
	lw   $a1, 4($t0)
	lw   $a2, 8($t0)
	jal  mult_matrix
	addi $v0, $zero, 10 # Call the Halt system call
	syscall # Halt the program


.globl mult_matrix
.ent mult_matrix
mult_matrix:
	# Register usage:
	# n is $s0, m is $s1, p is $s2,
	# r is $s3, c is $s4, i is $s5,
	# sum is $s6

	# Prologue
	sw   $fp, -4($sp)
	addi $fp, $sp, -4
	sw   $ra, -4($fp)
	sw   $s0, -8($fp)
	sw   $s1, -12($fp)
	sw   $s2, -16($fp)
	sw   $s3, -20($fp)
	sw   $s4, -24($fp)
	sw   $s5, -28($fp)
	sw   $s6, -32($fp)
	addi $sp, $sp, -36

	# Save arguments
	add $s0, $a0, $zero             # n
	add $s1, $a1, $zero             # m
	add $s2, $a2, $zero             # p

	addi  $s3, $zero, 0               # r = 0
	addi  $t0, $zero, 4               # sizeof(Int)

mult_loop:
	slt  $t1 , $s3, $s0
	beq  $t1 , $zero, mult_end  # if r >= n, branch
	addi $s4, $zero, 0               # c = 0

mult_loop2:
	slt  $t1 , $s4, $s2
	beq  $t1 , $zero, mult_end2 # if c >= p, branch
	addi $s6, $zero, 0               # int sum = 0;
	j    mult_loop3

mult_store:
	add  $a0, $s3, $zero
	add  $a1, $s2, $zero
	jal  mult_single
	add  $t3, $v0, $zero      # t3 = r * p

	add  $a0, $t3, $zero
	add  $a1, $t0, $zero
	jal  mult_single
	add  $t3, $v0, $zero	# t3 = t3 * 4

	add  $a0, $s4, $zero
	add  $a1, $t0, $zero
	jal  mult_single
	add  $t4, $v0, $zero	# t4 = c * 4       
	
	add  $t3, $t3, $t4        # t3 = t3 * t4 = (r * p * 4) + (c * 4)
	
	add  $t9, $zero, $zero
	addi $t9, 0x1001
	sll  $t9, $t9, 16
	addu $t9, $t9, $t3
	sw   $s6, 44($t9)          # C[r][c] = sum;

	addi $s4, $s4, 1          # c++

	addi $s5, $zero, 0               # i = 0
	j    mult_loop2

mult_loop3:
	slt  $t1 , $s5, $s1
	beq  $t1 , $zero, mult_store # if i >= m, branch

	# A[r][i]
	add  $a0, $s3, $zero
	add  $a1, $s1, $zero
	jal  mult_single
	add  $t5, $v0, $zero  # t5 = r * m

	add  $a0, $t5, $zero
	add  $a1, $t0, $zero
	jal  mult_single
	add  $t5, $v0, $zero  # t6 = i * 4

	add  $a0, $s5, $zero
	add  $a1, $t0, $zero
	jal  mult_single
	add  $t6, $v0, $zero  # t5 = (r * m * 4) + (i * 4)
  
	add  $t5, $t5, $t6        

	add  $t9, $zero, $zero
	addi $t9, 0x1001
	sll  $t9, $t9, 16
	addu $t9, $t9, $t5        
	lw   $t5, 12($t9)

	# B[i][c]

	add  $a0, $s5, $zero
	add  $a1, $s2, $zero
	jal  mult_single
	add  $t7, $v0, $zero  # t7 = i * n

	add  $a0, $t7, $zero
	add  $a1, $t0, $zero
	jal  mult_single
	add  $t7, $v0, $zero  # t8 = 4 * c

	add  $a0, $s4, $zero
	add  $a1, $t0, $zero
	jal  mult_single
	add  $t8, $v0, $zero  # t7 = (i * n * 4) + (c * 4)
   
	add  $t7, $t7, $t8

	add  $t9, $zero, $zero
	addi $t9, 0x1001
	sll  $t9, $t9, 16
	addu $t9, $t9, $t7        
	lw   $t7, 28($t9)

	add  $a0, $t5, $zero
	add  $a1, $t7, $zero
	jal  mult_single
	add  $t7, $v0, $zero  # t7 = t5 * t7

	add  $s6, $s6, $t7        # sum = sum + t7

	addi $s5, $s5, 1          # i++
	j    mult_loop3

mult_end2:
	addi $s3, $s3, 1          # r++
	j    mult_loop

mult_end:
	# Epilogue
	lw   $ra, -4($fp)
	lw   $s0, -8($fp)
	lw   $s1, -12($fp)
	lw   $s2, -16($fp)
	lw   $s3, -20($fp)
	lw   $s4, -24($fp)
	lw   $s5, -28($fp)
	lw   $s6, -32($fp)
	addi $sp, $s8, 4
	lw   $fp, ($fp)
	jr   $ra
.end mult_matrix

# multiplicação
.globl mult_single
.ent mult_single
mult_single:
	sw   $ra, -4($sp)
	sw   $s0, -8($sp)
	sw   $s1, -12($sp)
	sw   $t2, -16($sp)
	sw   $t3, -20($sp)
	addi $sp, $sp, -24

	# save arguments
	add $s0, $a0, $zero     # N
	add $s1, $a1, $zero 	# M
	addi $t3, $zero, 1

slt_mult:
	beq $s0, $zero, exit

	add  $t2, $s1, $t2
	addi $s0, $s0, -1
	j slt_mult

exit: 
	add  $v0, $t2, $zero
	addi $sp, $sp, 24
	lw   $ra, -4($sp)
	lw   $s0, -8($sp)
	lw   $s1, -12($sp)
	lw   $t2, -16($sp)
	lw   $t3, -20($sp)
	jr   $ra
.end mult_single