# sort.s
# Data declarations
.data
v: .word 4, 3, 5, 2, 1, 8, 10, 7, 11, 19, 20, 15, 6, 555, 333, 69, 24, 11, 47, 32 # data set to be sorted
n: .word 20 # Number of elements in the data set
.text
.align 2
.globl __start
__start:
	addi $t0, 0x1001
	sll  $t0, $t0, 16
	add $a0, $zero, $t0  # load the address of data set v in $a0 - 1st parameter
	lw $a1, 80($t0) # load the number of elements n in $a1 - 2nd parameter
	# Call sort routine
	jal sort # Call the sort routine
	# Stop program execution
	addi $v0, $zero, 10 # Call the Halt system call
	syscall # Halt the program


# Procedure SORT
.globl sort # make name sort global
.ent sort # define routine sort entry point
sort: addi $sp, $sp, -20 # make room on stack for 5 registers
	sw $ra,16($sp)# save $ra on stack
	sw $s3,12($sp) # save $s3 on stack
	sw $s2, 8($sp)# save $s2 on stack
	sw $s1, 4($sp)# save $s1 on stack
	sw $s0, 0($sp)# save $s0 on stack
	
	add $s3, $a1, $zero # copy parameter $a1 into $s3 (save $a1)
	add $s2, $a0, $zero # copy parameter $a0 into $s2 (save $a0)
	add $s0, $zero, $zero# i = 0

for1tst:slt $t0, $s0,$s3 #reg$t0=0if$s0Š$s3(iŠn)
	beq $t0, $zero, exit1# go to exit1 if $s0 Š $s3 (i Š n)
	addi $s1, $s0, -1# j = i – 1

for2tst:slti $t0, $s1,0 #reg$t0=1if$s1<0(j<0)
	bne $t0, $zero, exit2# go to exit2 if $s1 < 0 (j < 0)
	sll $t1, $s1, 2# reg $t1 = j * 4
	add $t2, $s2, $t1# reg $t2 = v + (j * 4)
	lw $t3, 0($t2)# reg$t3 = v[j]
	lw $t4, 4($t2)# reg$t4 = v[j + 1]
	
	sll $zero, $zero, 0 # nop
	
	slt $t0, $t4, $t3 # reg $t0 = 0 if $t4 Š $t3
	beq $t0, $zero, exit2# go to exit2 if $t4 Š $t3
	add $a0, $s2, $zero # 1st parameter of swap is v (old $a0)
	add $a1,$s1, $zero # 2nd parameter of swap is j
	jal swap # swap code shown in Figure 2.25
	addi $s1, $s1, -1# j –= 1
	j for2tst # jump to test of inner loop

exit2: addi $s0, $s0, 1 # i += 1
	j for1tst # jump to test of outer loop

exit1: lw $s0, 0($sp) # restore $s0 from stack
	lw $s1, 4($sp)# restore $s1 from stack
	lw $s2, 8($sp)# restore $s2 from stack
	lw $s3,12($sp) # restore $s3 from stack
	lw $ra,16($sp) # restore $ra from stack
	addi $sp,$sp,20 # restore stack pointer
	jr $ra
.end sort # End of routine sort code

# Swap subroutine
.globl swap # Make name swap global
.ent swap # define routine swap entry point
swap: sll $t1, $a1, 2 # reg $t1 = k * 4
	add $t1, $a0, $t1 # reg $t1 = v + (k * 4)
	# reg $t1 has the address of v[k]
	lw $t2, 4($t1) # reg $t2 = v[k + 1]
	lw $t0, 0($t1) # reg $t0 (temp) = v[k]
	# refers to next element of v
	sw $t2, 0($t1) # v[k] = reg $t2
	sw $t0, 4($t1) # v[k+1] = reg $t0 (temp)
	jr $ra # return from Swap to calling
.end swap # End of subroutine swap code